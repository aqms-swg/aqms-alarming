/**
* @file
* @ingroup group_alarming_sendalarm
*/
/***********************************************************

File Name :
        SendAlarm.h

Original Author:
        Patrick Small

Description:


Creation Date:
        22 October 1999

Modification History:
	12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef send_event_H
#define send_event_H

// Various include files
#include <stdint.h>
#include "Application.h"
#include "Connection.h"
#include "DatabaseAlarmEvent.h"


// Default configuration filename
const char DEFAULT_CONFIG[] = "./sendalarm.cfg";


class SendAlarm : public Application
{

 private:
    char tssfile[MAXSTR];
    uint32_t evid;
    char altype[MAXSTR];
    char alarmsubject[MAXSTR];
    Connection conn;
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;
    DatabaseAlarmEvent dbase;

    int _Cleanup();
    int _SendAlarmMessage();

 public:
    
    SendAlarm();
    ~SendAlarm();
    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
};


#endif
