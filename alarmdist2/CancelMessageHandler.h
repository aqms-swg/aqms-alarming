/**
* @file
* @ingroup group_alarming_alarmdist2
* @brief Header for CancelMessageHandler.C
*/
/***********************************************************

File Name :
        CancelMessageHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_ALARM_CANCEL messages.


Creation Date:
        04 October 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef cancel_msg_handler_H
#define cancel_msg_handler_H


// Various include files
#include "Connection.h"


// Function prototype for the alarm message handler
void handleCancelMsg(Message &m, void *arg);


#endif
