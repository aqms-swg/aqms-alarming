# alarmdist2

Modification to allow parallel execution of alarm actions (via unix select() capability) and periodic checking for new alarms independent of whether previous alarm scripts have returned (via select() timeout).  
  
