/**
* @file
* @ingroup group_alarming_alarmdist2
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

// int findChild(int pid);

/*
int main(int argc, char **argv)
{
   int  pid, childpid;

   pid = atoi(argv[1]);

   childpid = findChild(pid);

   printf("DEBUG> parent: %d   child: %d\n", pid, childpid);
   fflush(stdout);

   exit(0);
}
*/

int findChild(int refpid)
{
   int  pid, procpid, procppid;
   int  debug;

   char statfile[1024];
   char path    [1024];
   char procname[1024];
   char procstat[1024];

   char *end;

   debug = 0;


   // Open the /proc directory

   DIR    *dirp;
   struct  dirent *direntp;

   FILE *fstat;


   strcpy(path, "/proc");

   dirp = opendir(path);


   // Loop over <pid>/stat files in /proc

   while ( (direntp = readdir( dirp )) != NULL )
   {
      pid = strtol(direntp->d_name, &end, 10);

      if(end < direntp->d_name + strlen(direntp->d_name))
         continue;

      sprintf(statfile, "/proc/%d/stat", pid);

      fstat = fopen(statfile, "r");

      fscanf(fstat, "%d %s %c %d", &procpid, (char *)&procname, (char *)&procstat, &procppid);

      if(debug)
      {
         printf("DEBUG> Check ref: %d vs. parent: %d (child: %d %s)\n", refpid, procppid, procpid, procname);
         fflush(stdout);
      }

      if(procppid == refpid)
      {
         fclose(fstat);

         return procpid;
      }

      fclose(fstat);
   }


   // We didn't find any child with this parent

   return -1;
}
