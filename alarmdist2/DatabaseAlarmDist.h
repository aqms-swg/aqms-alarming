/**
* @file
* @ingroup group_alarming_alarmdist2
* @brief Header for DatabaseAlarmDist.C
*/
/***********************************************************

File Name :
        DatabaseAlarmDist.h

Original Author:
        Patrick Small

Description:


Creation Date:
        24 September 1999

Modification History:
		12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef database_alarmdist_H
#define database_alarmdist_H


// Various include files
#include <list>
#include <map>
#include <stdint.h>
#include "Action.h"
#include "Database.h"

// Definition of an action list
typedef std::list<Action, std::allocator<Action> > ActionList;

// Definition of an action map
typedef std::map<uint32_t, ActionList, std::less<uint32_t>, 
    std::allocator<std::pair<const uint32_t, ActionList> > > ActionMap;


class DatabaseAlarmDist : public Database
{
 private:

 protected:

 public:
    DatabaseAlarmDist();
    DatabaseAlarmDist(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseAlarmDist();

    int DoSendAlarms();
    int GetPending(ActionMap &al);
    int SetState(ActionMap &al);
    
	 int SetActionState(int evid, ActionList::iterator aref);
    
	 int GetEventActions(uint32_t evid, const char *action, 
			ActionList &al);
    int GetCancelled(ActionMap &al);
    int GetCancelProc(ActionMap &al, ActionMap &others);
    int GetUnknown(ActionMap &al);
};

#endif
