/**
* @file
* @ingroup group_alarming_alarmdist2
* @brief Starting point for alarmdist2
*/
/***********************************************************

File Name :
        Main.C

Original Author:
        Patrick Small

Description:

Creation Date:
        18 October 1999

Modification History:

	18 Feb 2003 - paulf added link to system_timeout() instead of system()
		to prevent one bad script from hanging the alarming.
		Also added versioning. starting with version v0.0.2

	27 Feb 2003 - paulf added check for kill of child by signal
		version v0.0.3

	08 Jan 2003 - paulf changed from RogueWave DB calls to OTL
		version v0.0.4

	23 Jan 2004 - paulf - removed dbschema requirement and usage
		version v0.0.5

	v0.0.6 2004-Feb-04 - recompiled with fixed Database.C MOD_DATE wrong
	v0.0.7 2004-04-01  - system_timeout() initialization bug fixed
	v0.0.8 2004-04-02  - bug found when alarmdist as shadow system
	v0.0.9 2004-Sep-24 - Added log messages for Event Logger.
	v0.0.10 2005-03-17 - added in EmailGroup and ScriptTimeout settings
			
			EmailGroup - gets emailed the stderr/stdout of a failed script
			ScriptTimeout - sets the number of seconds all scripts should complete within.
			Defaults to 60 seconds if not set.
 
	v0.0.11 2007-03-16 - added in "order by alarm_action, modcount desc" to order GetEventActions()
	v0.0.12 2013-01-17 - Ported to Linux
	v0.0.13 2014-03-13 - Modified GetEventActions() to not care if any actions were previously cancelled!
	v0.0.14 2014-03-18 - in AlarmdDistribution::_DoActions function waitpid for child processes left after 
                        a timeout by 'alternative' system_timeout2 function where function does not SIGKILL child,
                        just returns error -1 to allow child to complete

	v0.0.15 2017-07-21 - ADG: Modification to allow parallel execution
	                     of alarm actions (via unix select() capability) and periodic
	                     checking for new alarms independent of whether previous
	                     alarm scripts have returned (via select() timeout).
	v0.1.0  2018-09-19 - 64bit merge and postgres compilation db notification

Usage Notes:

**********************************************************/
#define VERSION "0.1.0 19-Sep-2018"
#ifdef USE_POSTGRES
const char* db_version = "Using Postgres SQL syntax";
#else
const char* db_version = "Using Oracle SQL syntax";
#endif


// Various include files
#include <iostream>
#include "RetCodes.h"
#include "GenLimits.h"
#include "RTRuntime.h"
#include "AlarmDistribution.h"

int debug = 0;

// Displays program usage information to the user
void usage(char *progname)
{
	std::cout << std::endl << "usage: " << progname << " <config file>" 
	          << std::endl << std::endl;
	std::cout << "Version " << VERSION << std::endl;
	std::cout << "DB:  " << db_version << std::endl;
	exit(0);
}  // end usage


// Main method
int main(int argc, char **argv)
{
	AlarmDistribution ad;

// Check for correct number of arguments
	if (argc != 2) 
	{
		usage(argv[0]);
		return(1);
	}

	std::cout << "Version v" << VERSION << std::endl;
	ad.SetProgramVersion(VERSION);

	RTRuntime run(&ad, argv[1]);

	if (!run) 
	{
		std::cout << "Error (main): Unable to create runtime environment" << std::endl;
		return(1);
	}

	if (run.Run() != TN_SUCCESS) 
	{
		std::cout << "Error (main): Run failure" << std::endl;
	}

	std::cout << "Terminating program" << std::endl;
	return(0);
}  // end main

