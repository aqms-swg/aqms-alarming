/**
* @file
* @ingroup group_alarming_alarmdist
* @brief Defines the routines needed to process incoming TN_TMT_HEARTBEAT messages.
*/
/***********************************************************

File Name :
        HeartbeatMessageHandler.C

Original Author:
        Kalpesh Solanki

Description:

        This source file defines the routines needed to process
incoming TN_TMT_HEARTBEAT messages.


Creation Date:
        21 July 2003

Modification History:


Usage Notes:


**********************************************************/

// Various include files
#include <iostream>
#include "RetCodes.h"
#include "HeartbeatMessageHandler.h"
#include "AlarmDistribution.h"


// Callback for Event Magnitude/Location/Archive messages
void handleHeartbeatMsg(Message &m, void *arg)
{
  AlarmDistribution *ad;
  RTStatusManager sm;

  ad = (AlarmDistribution *)arg;
  if (ad->GetStatusManager(sm) != TN_SUCCESS) {
    std::cout << "Error (handleActionMsg): Unable to retrieve status manager" 
	 << std::endl;
    return;
  }

  int id=0;
  // Unpack the heartbeat identifier
  if(m.next(id) != TN_SUCCESS) {
      printf("(handleEventMsg): Unable to unpack heartbeat identifier");
  }
  
  char msg[MAXSTR];
  sprintf(msg,"[HB] %d",id);
  sm.InfoMessage(msg);

}
