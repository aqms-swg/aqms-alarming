/**
* @file
* @ingroup group_alarming_alarmdist
* @brief Header for ActionMessageHandler.C
*/
/***********************************************************

File Name :
        ActionMessageHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_ACTION messages.


Creation Date:
        24 September 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef action_msg_handler_H
#define action_msg_handler_H


// Various include files
#include "Connection.h"


// Function prototype for the alarm message handler
void handleActionMsg(Message &m, void *arg);


#endif
