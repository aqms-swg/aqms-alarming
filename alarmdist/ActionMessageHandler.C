/**
* @file
* @ingroup group_alarming_alarmdist
* @brief Defines the routines needed to process incoming TN_TMT_ACTION_SIG messages.
*/
/***********************************************************

File Name :
        ActionMessageHandler.C

Original Author:
        Patrick Small

Description:

        This source file defines the routines needed to process
incoming TN_TMT_ACTION_SIG messages.


Creation Date:
        04 October 1999

Modification History:


Usage Notes:


**********************************************************/

// Various include files
#include <iostream>
#include "RetCodes.h"
#include "ActionMessageHandler.h"
#include "AlarmDistribution.h"


// Callback for Event Magnitude/Location/Archive messages
void handleActionMsg(Message &m, void *arg)
{
  AlarmDistribution *ad;
  RTStatusManager sm;

  ad = (AlarmDistribution *)arg;
  if (ad->GetStatusManager(sm) != TN_SUCCESS) {
    std::cout << "Error (handleActionMsg): Unable to retrieve status manager" 
	 << std::endl;
    return;
  }

  sm.InfoMessage("Received action signal");

  if (ad->DoAction() != TN_SUCCESS) {    
    sm.ErrorMessage("(handleActionMsg): Unable to distribute alarm actions");
    return;
  }

  return;
}
