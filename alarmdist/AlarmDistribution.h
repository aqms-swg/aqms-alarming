/**
* @file
* @ingroup group_alarming_alarmdist
* @brief Header for AlarmDistribution.C
*/
/***********************************************************

File Name :
        AlarmDistribution.h

Original Author:
        Patrick Small

Description:


Creation Date:
        04 October 1999

Modification History:
		12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef alarm_distribution_H
#define alarm_distribution_H

// Various include files
#include <stdint.h>
#include "RTApplication.h"
#include "DatabaseAlarmDist.h"

#define ALARMACTION_TIMEOUT 60	/* this is the number of seconds a script has to complete!
				 	if it does not, system_timeout() will kill it and
					move on */


// Definition of the valid operating modes
typedef enum {AD_MODE_NONE, AD_MODE_RT, AD_MODE_DC} opMode;


class AlarmDistribution : public RTApplication 
{
 private:
    char heartbeatsubject[MAXSTR];
    char actionsubject[MAXSTR];
    char cancelsubject[MAXSTR];
    char scriptdir[MAXSTR];
    char email_group[MAXSTR];		// added for emailing upon error in script
    opMode mode;
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;
    int script_timeout;

    int _Cleanup();
    int _GetDatabase(DatabaseAlarmDist &db);
    int _DumpActions(ActionMap &al);
    int _DumpActions(uint32_t evid, ActionList &al);
    int _DoActions(DatabaseAlarmDist &db, ActionMap &al);
    int _UndoActions(DatabaseAlarmDist &db, uint32_t evid,
		     ActionList &al);
    int _CancelPending(DatabaseAlarmDist &db, uint32_t evid,
		       ActionList &al);
    int _SetState(DatabaseAlarmDist &db, ActionMap &al, actionState newstate);
    int _CheckConsistency();
    int _EmailScriptError(char *script, char *stdout_filename, char *stderr_filename, char *err_msg);

 public:

    AlarmDistribution();
    ~AlarmDistribution();
    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
    int DoAction();
    int DoCancel(uint32_t evid, const char* action);
};


#endif
