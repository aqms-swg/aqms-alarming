# sendcancel {#man-sendcancel}
Initiate a cancellation of an particular alarm, or all alarms for a given event.  
  
   
## PAGE LAST UPDATED ON  

2021-01-20  
  

## NAME  

sendcancel  



## VERSION and STATUS  
v0.0.9 2017-12-27  
status: ACTIVE  
  

## PURPOSE  
sendcancel sends a CMS signal to alarmdist to cancel an action after it checks that there are any actions in the database related to an event. The user can cancel a specific alarm or use the wild card * character to cancel all alarms. The wild card char must be escaped with single quotes when used with the -a option.  
  

## HOW TO RUN  
```
sendcancel -c sendcancel.cfg -a action evid
```
where the *sendcancel.cfg* is described below, the *action* is the name of a specific action (case sensitive) and the *evid* is the integer event id of the event needing actions cancelled on.  


## CONFIGURATION  

All configuration parameters are **required**  

Format is 
```
<parameter name> <data type> <default value>  
<description>
```  

**General configuration parameters**  

**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored. The actual files will have "_YYYYMMDD.log" appended to this path, where YYYYMMDD is the four-digit year, month and day.  
  
**LoggingLevel** *integer*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  
  
**LogFile** *integer*  1  
If 0, no logfile is generated, if 1, then a earthworm style logfile is created  
  
**ProgramName** *string* SENDCANCEL  
The unique name to allow this module to connect to CMS and be identifed.  
  

**Application configuration parameters**  
  

**CMSConfig** *string* cms.cfg  
Points to the cms configuration file which holds all CMS details.  
  
**CancelSubject** *string* /signals/actions/cancel  
The name of the CMS signal to use for cancellation that alarmdist listens to.  
  
**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.  
  
**DBPasswd** *string*  
The password for the database user account specified by DBUser.  


A sample configuration file is shown below:  
```
#
# General configuration parameters
#
Logfile         ./sendcancel.log
LoggingLevel    2
ProgramName     SEND_CANCEL

#
# Application configuration parameters
#
CMSConfig                       ./sendcancel_cms.cfg
CancelSubject                   /signals/actions/cancel
DBService                       somename
DBUser                          someuser
DBPasswd                        somepassword
```  
  
  
## DEPENDENCIES  
  
The sendcancel module depends on CMS being running and the Database being available and alarm_action table having some actions for the event id to actually be cancelled.  
  
## MAINTENANCE  
  
No specific advice other than to check the log file.  
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-alarming/-/issues  
  
## MORE INFORMATION  
  
Authors : Patrick Small, Kalpesh Solanki   

Don't be mislead by the **-F** flag that sendcancel will offer as a way to *FORCE* a cancellation. All that does is to bypass sendcancel's database check. But if the event actions that you are trying to cancel cannot be found by alarmdist, your use of sendcancel will have no affect regardless of the presence of absence of the **-F** flag.  
  
[alarmdec](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdec.html), [alarmact](https://aqms-swg.gitlab.io/aqms-docs/man-alarmact.html), [alarmdist](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdist.html) are the chain of alarming programs. sendcancel sends a signal to alarmdist but uses the action name found in alarmact.















