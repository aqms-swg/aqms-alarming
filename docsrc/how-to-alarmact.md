How to compile and install alarmact {#how-to-compile-install-alarmact}
====================================

# Pre-requisites

alarmact depends on aqms-libs, so ensure that they are compiled and available before compiling it.

# Compile

```
make -f Makefile
```
This will create a binary named alarmact.

# Install

At your preferred location, create a folder named *alarm*. Copy binary *alarmact* under *alarm*.  
Under alarm create a folder named *configs*. This will hold all alarm configs.  Copy *alarmact.cfg* here.



# Configure

Modify the sample *alarmact.cfg* as per your equirements.

Ensure that *Logfile* is available and writable to by alarmact.

Ensure that the database is running and reachable by alarmact.