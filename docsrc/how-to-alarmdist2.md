How to compile and install alarmdist2 {#how-to-compile-install-alarmdist2}
====================================

# Pre-requisites

alarmdist2 depends on aqms-libs, so ensure that they are compiled and available before compiling it.

# Compile

```
make -f Makefile
```
This will create a binary named *alarmdist*. Rename it to alarmdist2. 

# Install

At your preferred location, create a folder named *alarm*. Copy binary *alarmdist2* under *alarm*.  
Under alarm create a folder named *configs*. This will hold all alarm configs.  Copy *alarmdist2.cfg* here.



# Configure

Modify the sample *alarmdist2.cfg* as per your equirements.

Ensure that *Logfile* is available and writable to by alarmdist2.

Ensure that the database is running and reachable by alarmdist2.