How to compile and install telestifle {#how-to-compile-install-telestifle}
====================================

# Pre-requisites  

Telestifle depends on :  

* aqms-libs, so ensure that they are compiled and available before compiling it.
* perl scripts *telesListener.pl* and *telesNotify.pl* depend on Qtime perl module. ???
* Taup jar files. https://www.seis.sc.edu/taup/
* Product Distribution Layer (PDL). https://usgs.github.io/pdl/

# Compile  

Download the Taup jar files. Modify the variable TAUP_HOME in the Makefile to point to this location.  

Run :  

```
make -f Makefile
```
This will create a binary named *telestifle*.

# Install

At your preferred location, create a folder named *alarm*. Under alarm create a folder named *configs*. This will hold all alarm configs.  
* Copy binary *telestifle* under *alarm*.  
* Copy the sample *taup-jar.env*, *telestifle.cfg*, *telestifle_rules.cfg* and *telestifle_points.cfg* here.
* Under *alarm*, create a folder named *FIFO* and under it a file named *telesFIFO*. This path should be the value for parameter *InputFIFO* in telestifle.cfg
* Under *alarm*, create a folder named *msgs* and copy the sample perl scripts *telesListener.pl* and *telesNotify.pl* under *msgs*.  
* Under *alarm*, create a folder named *jars* and copy the downloaded Taup jar files under *jars*. Modify *taup-jar.env* with the correct paths.
  
To setup PDL for  telestifle, do the following in the folder containing your ProductClient.jar  
* Copy *init-teles.sh* and the sample *config-teles.ini*. Modify *config-teles.ini* as needed.




# Configure

Modify all the sample *.cfg* as per your equirements. 

Ensure that *Logfile* is available and writable to by telestifle.

Ensure that the database is running and reachable by telestifle.