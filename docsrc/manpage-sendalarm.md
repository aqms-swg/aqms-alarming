# sendalarm {#man-sendalarm}
Send an alarm message of the type specified on the command line. 
  
   
## PAGE LAST UPDATED ON  

2021-01-22  
  

## NAME  

sendalarm  



## VERSION and STATUS  
v0.0.5 17-Jan-2013 
status: ACTIVE  
  

## PURPOSE  
Send an alarm message of the type specified on the command line.    
  

## HOW TO RUN  
```
sendalarm -c sendalarm.cfg evid alarmname
```
where the *sendalarm.cfg* is described below, the *alarmname* is the name of a specific alarm and the *evid* is the integer event id of the event needing the alarm sent on.  


## CONFIGURATION  

All configuration parameters are **required**  

Format is 
```
<parameter name> <data type> <default value>  
<description>
```  

**General configuration parameters**  

**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored. The actual files will have "_YYYYMMDD.log" appended to this path, where YYYYMMDD is the four-digit year, month and day.  
  
**LoggingLevel** *integer*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  
  
**LogFile** *integer*  1  
If 0, no logfile is generated, if 1, then a earthworm style logfile is created  
  
**ProgramName** *string* SEND_ALARM  
The unique name to allow this module to connect to CMS and be identifed.  
  

**Application configuration parameters**  
  

**CMSConfig** *string* cms.cfg  
Points to the cms configuration file which holds all CMS details.  
  
**AlarmSubject** *string* /signals/alarmdec/alarm  
The name of the CMS signal to use that alarmdist listens to.  
  
**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.  
  
**DBPasswd** *string*  
The password for the database user account specified by DBUser.  


A sample configuration file is shown below:  
```
#
# General configuration parameters
#
Logfile         ./sendalarm.log
LoggingLevel    2
ProgramName     SEND_ALARM

#
# Application configuration parameters
#
CMSConfig                       ./sendalarm_cms.cfg
AlarmSubject                   /signals/alarmdec/alarm
DBService                       somename
DBUser                          someuser
DBPasswd                        somepassword
```  
  
  
## DEPENDENCIES  
  
The sendalarm module depends on CMS being running and the Database being available.
  
## MAINTENANCE  
  
No specific advice other than to check the log file.  
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-alarming/-/issues  
  
## MORE INFORMATION  
  
Authors : Patrick Small, Kalpesh Solanki, Paul Friberg, Pete Lombard  
  














