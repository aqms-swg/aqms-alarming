# alarmdec {#man-alarmdec}

Alarmdec, short for Alarm Decision, decides which alarms are to be acted on by alarmact, the next module in the alarming chain   

## PAGE LAST UPDATED ON

2020-05-27

## NAME

alarmdec

## VERSION and STATUS

v0.2.0 2018-09-19  
status: ACTIVE  
 

## PURPOSE

alarmdec is short for Alarm Decision, basically it decides which alarms are to be acted on by alarmact, the next module in the alarming chain. Typically alarmdec is signaled from a magnitude generator or from a hypocenter generator that writes event parameters into the database. Alarmdec receives a CMS message from the generator or the alarmevent message that contains the event id (evid) of the event to declare alarms on.  
	
In alarmdec, an alarm consists of an alarm name, together with zero or more criteria. Each of the criteria must pass in order for the alarm to be declared. When an alarm is declared for an event, a CMS message containing the event ID and the alarm name is published. Normally it is the alarmact module which subscribes to these messages and maps the alarm name into one or more alarm actions. Note that alarmdec does not write anything to the database.  
		
In order to declare multiple alarms that have different triggering sequences, you must configure an alarmdec per triggering sequence. For example, you might want to have one set of alarms happen after a local magnitude is computed, and a different set of alarms after ampgen has completed.  
			
An event is not required to have a magnitude at the time that alarmdec is called to evaluate the event. However, if no magnitude is found for the event in the database, alarmdec will treat the event as if it had a magnitude of type 'n' (no magnitude) and of value 0.  
			
A given instance of alarmdec may be configured for multiple alarms. Each of these alarms is evaluated independently. The success or failure of an event to meet one alarm has no effect on the other alarms.  
				
For alarmdec version 0.1.5 (9 November 2012) or later, alarmdec can be configured to stifle some alarms during the passage of the P wave from a teleseism. alarmdec can be configured to subscribe to CMS messages from the telestifle program. These messages contain the start and end times of the estimated P wavefront passage. Event origins with times inside this window will be evaluated using a alternate set of alarm criteria. See Stifling Alarms during Teleseisms for more information. Each event which is evaluated against the alternate rule set will have a log entry announcing that fact.  
					

## HOW TO RUN

```
alarmdec alarmdec.cfg
```
alarmdec.cfg is described below. Alarmdec uses two additional config files; alarmdec_alarms.cfg and alarmdec_regions.cfg, both of which are described below.  

## CONFIGURATION FILE
Format of the config file is

```
<parameter name> <data type> <default value>
<description>
```

**Include** *string*  
External file to be included, with path prefix.  

**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored.  

**LoggingLevel** *number*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  

**ProgramName** *string* &nbsp;&nbsp;ALARM_DEC  
The unique name to allow this module to connect to CMS and be identifed.  

**HSInterval** *number*  
How often in seconds to send a heartbeat signal to CMS.  

**TSSOption** OR **CMSConfig** *string*  
Points to the cms configuration file which holds all CMS details. In addition, the common CMS signals should be in the config file.  


**Application configuration parameters**

**HeartbeatSubject** *string*  
The heartbeat CMS signal that the module writes (note this is not used by any CMS receiver yet but is required).   

**AlarmFile** *string* &nbsp;&nbsp;alarmdec_alarms.cfg  
The configuration of which alarms use what decision criteria is specified in this file. See below for a desription of the format of this file.  

**RegionFile** *string* &nbsp;&nbsp;alarmdec_regions.cfg  
This configuration file provides the alarms location decision criteria to decide if a given event is located within a given polygon on the earth.  

**EventSubject** *string*  
The name of the CMS signal to use for...  

**AlarmSubject** *string*  
The name of the CMS message to use for invoking the next module in the alarming thread (the one that alarmact should listen for). The CMS message sent contains the evid and the alarm name.  

**UpdateSubject** *string* &nbsp;&nbsp;/signals/events/update  
The name of the CMS signal to use for alarms indicating a change in an event. In reality this is handled no differently than the EventSubject type of notification messages.  


**Optional commands for teleseism stifling**  

**AltAlarmFile** *string*  
The configuration used for events with origin times within a teleseism window of which alarms use what decision criteria. The format is the same as for alarmdec_alarmcs.cfg.  

**TelesSubject** *string*  
the name of the CMS message which provides the start and end times of the teleseism P wave front passage across your network.  


**Database parameters**  

**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.  

**DBPasswd** *string*  
The password for the database user account specified by DBUser.  

**DBConnectionRetryInterval** *number*  
Time to wait, in seconds, before re-attempting connection to the database.  

**DBMaxConnectionRetries** *number*  
Max number of times to attempt database connection.  

*A sample alarmdec.cfg file is shown below*  

```
#
# General configuration parameters
#
TSSOption       ../cms/conf/cms.cfg
Include         ../cms/conf/common_signals.cfg
Logfile         ../rt/logs/alarmdec
LoggingLevel    2
ProgramName     ALARM_DEC
HSInterval      30


#
# Application configuration parameters
#
HeartbeatSubject                /signals/alarms/heartbeat
AlarmFile                       ./configs/alarmdec_alarms.cfg
RegionFile                      ./configs/alarmdec_regions.cfg
EventSubject                    /signals/alarmevent/new
UpdateSubject                   /signals/alarmevent/update
AlarmSubject                    /signals/alarmdec/alarm

#db info
Include                         ../db/db_info.d

DBMaxConnectionRetries          10 
```

**alarmdec_alarms.cfg** is the configuration file that governs the criteria to be used in making an alarm activate.  

Alarms are specified by an unique alarm name followed by curly brackets around any number of criteria:  
```
alarm_name_1 {
	criteria1 args...
	criteria2 args...
	...
	criterian args...
	}
```

The alarm name is passed along to identify the actions associated with the alarm in alarmact.   

Criteria can be one or more of:  

* **Region** *name* - region named in alarmdec_regions file, the earthquake must be located within the region specified. 
* **DepthRange** *min max* - depth >= min and < max 
* **NumLocPhasesRange** *min max* - Number of locating phases >= min and < max 
* **NumLocPhases** *min* - at least "min" locating phases
* **LocRMSRange** *min max* - origin RMS >= min and < max
* **LocRMS** *max* - origin RMS < max
* **MaxGapRange** *min max* - maximum azimuthal >= min and < max
* **ReviewFlags** *xyz* - Allowed review flags are one of x, y, or z (Known flags: A - automatic; H - human; F - final)
* **MagTypes** *x,y,z* - magnitude is of type x or y or z
    * Specify mag types as a comma-separated list
    * Permitted values: un,n,a,b,e,l,l1,l2,lg,c,s,w,z,B,d,h 
	* See Schema documentation about netmag.magtype for further details.
* **MagRange** *min max* - magnitude >= min and < max 
* **NumMagStationsRange** *min max* - number of magnitude stations >= min and < max
* **NumMagStations** *min* - number of magnitude stations >= min 
* **MagRMSRange** *min max* - magnitude RMS >= min and < max 
* **MagRMS** *max* - magnitude RMS < max
* **MagQualityRange** *min max* - magnitude quality >= min and < max
* **EventAge** *max_hours* - maximum event age in hours since origin time 
* **TimeOfDay** *start end* - alarm is active between start and end times, where times are hours and minutes (HH:MM) in UTC time. 
* **OriginAuth** NN,CI,... - only alarm if the origin.auth network code matches one in the comma separated list. 

If a criterion is not mentioned, any value of that quantity will qualify for that alarm.  
 
If no criteria are configured for a given alarm, the alarm will be satisfied for every event.  


**alarm_regions.cfg** is the regions configuration file and should look like the example below and each polygon should end with the start point. The name specified is used in the Region criteria as an argument.  

```
# Note that these regions must be closed. The first point, must
# be repeated at the end of the region definition
#

Anywhere {
    Point    25.000   -200.000
	Point    25.000   -100.000
	Point    50.000   -100.000
	Point    50.000   -200.000 
	Point    25.000   -200.000
}
					
Southern_California {
	Point 38.00 -117.00
	Point 35.00 -121.00
	Point 34.65 -121.50
	Point 32.00 -119.00
	Point 32.28 -114.00
	Point 35.80 -114.00
	Point 38.00 -117.00
}
```

## ENVIRONMENT

NA

## DEPENDENCIES

The alarmdec module depends on CMS being running and the Database being available and populated with the event to be alarmed on.   

## MAINTENANCE

No specific advice other than to check the log file.  

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-alarming/-/issues

## MORE INFORMATION

Authors: Patrick Small, Kalpesh Solanki, Paul Friberg, Peter Lombard   

alarmdec, alarmact, alarmdist are the chain of alarming programs. alarmevent can be used to trigger an alarmdec decision process. sendcancel can be used to cancel an alarm.  
