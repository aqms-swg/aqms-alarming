# alarmdist2 {#man-alarmdist2}

alarmdist2, short for Alarm Distribution v2, dispatches alarm messages to users.

## PAGE LAST UPDATED ON

2021-01-26

## NAME

alarmdist2

## VERSION and STATUS

Version 0.1.0 2018-09-19  
status: ACTIVE  

## PURPOSE

Alarmdist2 is an alternative to alarmdist. Please see manpage for [alarmdist](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdist.html). 

Alarmdist2 is a modified alarmdist to allow parallel execution of alarm actions (via unix select() capability) and periodic checking for new alarms independent of whether previous alarm scripts have returned (via select() timeout). The rest of the functionality remains the same as alarmdist. 

					

## HOW TO RUN

```
alarmdist2 alarmdist2.cfg
```
alarmdist2.cfg is described below.  

## CONFIGURATION FILE
```
<parameter name> <data type> <default value>
<description>
```

**General configuration parameters**  

**Include** *string*  
External file to be included, with path prefix.  

**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored.  

**LoggingLevel** *number*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  

**ProgramName** *string* &nbsp;&nbsp;ALARM_DIST  
The unique name to allow this module to connect to CMS and be identifed.  

**HSInterval** *number*  
How often in seconds to send a heartbeat signal to CMS.  

**CMSConfig** *string*  
Points to the cms configuration file which holds all CMS details. In addition, the common CMS signals should be in the config file.  
  
**SystemSubject** *string* &nbsp;&nbsp;&nbsp; /system/control  

**StatusSubject** *string* &nbsp;&nbsp;&nbsp; /process/reports/status  

**DebugSubject** *string* &nbsp;&nbsp;&nbsp; /process/reports/debug  

**InfoSubject** *string* &nbsp;&nbsp;&nbsp; /process/reports/info  

**ErrorSubject** *string* &nbsp;&nbsp;&nbsp; /process/reports/error  


**Application configuration parameters**  

**HeartbeatSubject** *string*  /signals/alarms/heartbeat
The heartbeat CMS signal that the module writes (note this is not used by any CMS receiver yet but is required).  

**ActionSubject** *string*  /signals/actions/test
The name of the CMS signal to use for springing into action (sent from alarmact).   

**CancelSubject** *string*  /signals/actions/cancel/test
The name of the CMS signal to use for springing into action for a cancellation of an alarm (sent from sendcancel).  

**Mode** *string*  
The mode of operation RT or DC (realtime or data center).  

**ScriptDir** *string*  
The directory where scripts can be found. It can be relative to where the program is executed or an absolute path.  


**Database parameters**  

**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.  

**DBPasswd** *string*  
The password for the database user account specified by DBUser.  

**DBConnectionRetryInterval** *number*  
Time to wait, in seconds, before re-attempting connection to the database.  

**DBMaxConnectionRetries** *number*  
Max number of times to attempt database connection.  

*A sample alarmdist2.cfg is shown below*  

```
#
# General configuration parameters
#
CMSConfig       ./cms.cfg
SystemSubject   /system/control
StatusSubject   /process/reports/status
DebugSubject    /process/reports/debug
InfoSubject     /process/reports/info
ErrorSubject    /process/reports/error
Logfile         ./alarmdist
LoggingLevel    2
ProgramName     ALARM_DIST
HSInterval      30


#
# Application configuration parameters
#
HeartbeatSubject                /signals/alarms/heartbeat
ActionSubject                   /signals/actions/test
CancelSubject                   /signals/actions/cancel/test
Mode                            RT
ScriptDir                       ./alarms
DBService                       some_dbname
DBUser                          some_user
DBPasswd                        some_password
DBConnectionRetryInterval       5
DBMaxConnectionRetries          5

```



## ENVIRONMENT

NA

## DEPENDENCIES

The alarmdist2 module depends on CMS being running and the Oracle Database being available and populated with the event to be taken action on in the alarm_action table.  

## MAINTENANCE

No specific advice other than to check the log file and the database table for actions. Here is the schema information for the alarm_action table:

```
SQL> desc alarm_action
Name                                      Null?    Type
----------------------------------------- -------- ----------------------------
EVENT_ID                                  NOT NULL NUMBER(15)
ALARM_ACTION                              NOT NULL VARCHAR2(15)
ACTION_STATE                              NOT NULL VARCHAR2(15)
MODCOUNT                                  NOT NULL NUMBER(8)
MOD_TIME                                           DATE	   
```

Alarmdist2 uses the peer_health tables to see if the peer is operational. It could be used by RSN's to assure that alarms go out if the primary RT system's health is determined to be poor (by either itself or by the peer server). The peer or primary would write into the shadow datbase into the peer_system_status table that the peer is 'DOWN' and not 'OPERATIONAL'. For now, we simply declare both as statically operational with the following SQL command:   

```
insert into peer_system_status values ('OPERATIONAL', sysdate);
```

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-alarming/-/issues  


## MORE INFORMATION

Authors: Andrew Good, Patrick Small, Kalpesh Solanki, Paul Friberg, Peter Lombard  

alarmdec, alarmact, alarmdist2 are the chain of alarming programs. alarmevent can be used to trigger an alarmdec decision process.  

