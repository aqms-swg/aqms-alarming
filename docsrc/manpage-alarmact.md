# alarmact  {#man-alarmact}

alarmact is short for Alarm Action, basically it decides which actions to take for a given alarm raised by alarmdec.

## PAGE LAST UPDATED ON

2020-05-29

## NAME

alarmact

## VERSION and STATUS

v0.1.0 2018-09-20  
status: ACTIVE  

## PURPOSE

alarmact is short for Alarm Action, basically it decides which actions to take for a given alarm raised by alarmdec. Alarmact receives a CMS message from the alarmdec module and the message contains the event id (evid) and an alarm name. Upon receiving a CMS message it evaluates its list of alarms and the actions to initiate for it. For each action to be initiated, the program writes the action into the database alarm_action table and then notifies the alarm distribution module (alarmdist) via a CMS message that it should run on this event's actions. The CMS message simply contains the event id.
	
The alarm_action database table has 5 fields, the event_id, alarm_action, action_state, modcount, and mod_time. When alarmact writes a new row into this table for each action and the action_state is set to ACTION_STATE_PENDING for a new action. It sets the modcount to +1 from the highest modcount for any previous action that matches the same alarm_action name and event_id. alarmdist, the next program in the chain then reads this table and processes the actions found based on the action_state.
		

## HOW TO RUN

```
alarmact alarmact.cfg
```

## CONFIGURATION FILE

The configuration file alarmact.cfg and is described below. Format is  
```
<parameter name> <data type> <default value>
<description>
```

**General configuration parameters**  

**Include** *string*  
External file to be included, with path prefix.  

**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored.  

**LoggingLevel** *number*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  

**TSSOption** or **CMSConfig** *string*  
Points to the cms configuration file which holds all CMS details. In addition, the common CMS signals should be in the config file.  

**ProgramName** *string* &nbsp;&nbsp;ALARM_ACT  
The unique name to allow this module to connect to CMS and be identifed.  

**HSInterval** *number*  
How often in seconds to send a heartbeat signal to CMS.  

**Application configuration parameters**  

**HeartbeatSubject** *string*  
The heartbeat CMS signal that the module writes (note this is not used by any CMS receiver yet but is required).   

**AlarmSubject** *string*  
The name of the CMS signal to use for invoking the next module in the alarming thread (the one that alarmact should listen for). The CMS message sent contains the evid and the alarm name.  

**ActionSubject** *string*  
The name of the CMS signal to use for evaluating actions to be performed for a given alarm (sent from alarmdec).  

**ActionFile** *string*  &nbsp;&nbsp;alarmact_actions.cfg  
This configuration file provides the actions that should be fired for a given alarm name.  

**Database connnection parameters**  

**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.  

**DBPasswd** *string*  
The password for DBUser  

**DBConnectionRetryInterval** *number*  
Time to wait, in seconds, before re-attempting connection to the database.  

**DBMaxConnectionRetries** *number*  
Max number of times to attempt database connection.  

*A sample alarmact.cfg is shown below.*  

```
#
# General configuration parameters
#
TSSOption       ../run/cfg/cms/cms.cfg
Include         ../run/cfg/cms/common_signals.cfg
Logfile         ../run/logs/alarmact
LoggingLevel    2
ProgramName     ALARM_ACT
HSInterval      30


#
# Application configuration parameters
#
HeartbeatSubject                /signals/alarms/heartbeat
AlarmSubject                    /signals/alarmdec/alarm
ActionSubject                   /signals/alarmact/action

ActionFile                      ./configs/alarmact_actions.cfg

# db include
Include                         ../db/db_info.d
```

**alarmact_actions.cfg** is the configuration file that governs the actions that should be invoked for a given alarm name sent by alarmdec.  
 
Actions for a given alarm are specified by an unique alarm name followed by parenthesis actions that should be invoked for this alarm:  

```
alarm_name_1 {
    Action action1
	Action action2
	...
	Action actionN
}
```

The alarm name is passed along from alarmdec and must match an alarm decision name. The actions names are strings that will be used by alarmdist to fire scripts of the same name, so these action names should be written with care. See the alarmdist module for details.  
 
Here is an actual alarmact config file provided with a stock configuration of the system:  

```
# test of alarmact, note COMP email sent only.
#
#  WEBMAP alarms resolve to sending CUBE comp pages to QDDS
#

NORTH_WEBMAP {
	Action CUBE_Comp_Email
}
		
SOUTH_WEBMAP {
	Action CUBE_Comp_Email
}				
```
## ENVIRONMENT

NA

## DEPENDENCIES

The alarmact module depends on CMS being running and the Oracle Database being available and populated with the event to be taken action on.  
	

## MAINTENANCE

No specific advice other than to check the log file and the database table for actions. Here is the schema information for the alarm_action table:

```
SQL> desc alarm_action
Name                                      Null?    Type
----------------------------------------- -------- ----------------------------
EVENT_ID                                  NOT NULL NUMBER(15)
ALARM_ACTION                              NOT NULL VARCHAR2(15)
ACTION_STATE                              NOT NULL VARCHAR2(15)
MODCOUNT                                  NOT NULL NUMBER(8)
MOD_TIME                                           DATE
```		   

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-alarming/-/issues

## MORE INFORMATION

Authors : Patrick Small, Kalpesh Solanki, Paul Friberg, Peter Lombard   

[alarmdec](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdec.html), alarmact, [alarmdist](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdist.html) are the chain of alarming programs. [alarmevent](https://aqms-swg.gitlab.io/aqms-docs/man-alarmevent.html) can be used to trigger an alarmdec decision process.  
