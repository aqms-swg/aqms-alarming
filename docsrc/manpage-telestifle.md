# telestifle {#man-telestifle}
Initiates stifling of alarms due to a teleseismic earthquake.  
  
   
## PAGE LAST UPDATED ON  

2021-01-25  
  

## NAME  

telestifle  


## VERSION and STATUS  
0.0.3 2015-Dec-03  
status: ACTIVE  
  

## PURPOSE  

telestifle is a program to assist in the *stifling* of alarms due to a teleseismic earthquake. When the P phases of a large, (usually) deep teleseismic earthquake pass through a regional seismic network, phase pickers and associators sometimes falsely declare local earthquakes from these teleseismic arrivals. Upon learning of a teleseism, telestifle evaluates the event against rule sets. If the teleseism passes one or more of the rule sets, it estimates the times when selected phase arrivals will pass through a set of points surrounding the network. telestifle sends the time window to alarmdec on the RT systems. telestifle can also be configured to notify duty response personnel about qualifying teleseismic earthquakes.  

telestifle does not itself detect teleseisms. Instead it gets notified of them by some other program. In the Northern California Seismic System (NCSS), we use the Product Distribution Layer (PDL) (documentation at http://ehppdl1.cr.usgs.gov) system to provide these notifications. Included in the telestifle source directory are a sample PDL listener **TEMPLATE-telesListener.pl** and a sample PDL configuration file **TEMPLATE-exec.ini**. The input to telestifle is a UNIX FIFO (a file system device also known as a Named Pipe), specified in the telestifle configuration file as *InputFIFO*. Note that telestifle and its notification must agree on the location as this FIFO. The FIFO is created by telestifle when it starts up. These sample files will have to be modified for local use.  

telestifle uses Philip Crotwell's TauP traveltime calculator, available at http://www.seis.sc.edu/TauP/. The C source code to access this package is included with telestifle; the java code is NOT included. The TauP package includes several velocity models within the jar files; special configuration of TauP is generally not required to telestifle.  
  
telestifle can be configured to notify interested parties about the impending passage of the P wavefront over the network. This is done in telestifle configuration parameter **NotifyProgram**. (If you don't want any notification, configure NotifyProgram with something innocuous such as /bin/true/). Included in the source directory is **TEMPLATE-telesNotify**, a sample of the aforementioned script . This sample will have to be modified for local use.   
 
**Real-time Runtime Telestifle Setup**

The script used to start and stop the PDL receiver is **init-teles.sh**, found in the ProductClient.jar home directory */app/aqms/ProductClient*. This script reads a configuration file, a template for this **TEMPLATE-config-teles.ini** is included in the source code. The received origin products are stored in the *data/teles_exec_storage* subdirectory. Log files are written to the *log/teles* subdirectory. A cleanup cronjob script is setup to delete old storage products which otherwise accumulate on the disk, the crontab entry is: *1 0 \* \* 0 /app/aqms/ProductClient/teles-origin-cleanup.sh > /dev/null 2>&1*

The telestifle alarm module which listens for the PDL notification message is started using the **telesctl.sh** script found in the alarm home directory */app/aqms/alarm*. On startup, the file *configs/taup-jar.env* is sourced which defines the CLASSPATH variable listing java jars files found in the alarm home *jars* subdirectory. These jars are used by telestifle to calculate the P phase travel times.

The main configuration file used by telestifle is ```telestifle.cfg``` and is found in the alarm home configs directory. The remaining configuration files required by telestifle, located also under alarm home configs, are:

* ```telestifle_points.cfg``` :
        list of points defining the network boundary used to determine the P-phase travel time window.
* ```telestifle_rules.cfg``` :
        lists the distance, magnitude, and depth ranges allowed for an event to be eligible for a phase travel time calculation.
 
These are described in the Configuration sections.  

## HOW TO RUN  
```
telestifle telestifle.cfg
```
where the *telestifle.cfg* is described below.


## CONFIGURATION  


Format is 
```
<parameter name> <data type> <default value>  
<description>
```  

### telestifle.cfg  
  

**General configuration parameters**  

**CMSConfig** *string* cms.cfg  
Points to the cms configuration file which holds all CMS details.   

**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored. The actual files will have "_YYYYMMDD.log" appended to this path, where YYYYMMDD is the four-digit year, month and day.  
  
**LoggingLevel** *integer*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  
   
**ProgramName** *string* TELES_STIFLE  
The unique name to allow this module to connect to CMS and be identifed.  
  

**Application configuration parameters**  
  
**RulesFile** *string* &nbsp;&nbsp;&nbsp;&nbsp;./configs/telestifle_rules.cfg  
The configuration of rules for evaluating teleseisms. See below for a description of the format of this file.  
  
**PointsFile** *string*     ./configs/telestifle_points.cfg  
The configuration file which provides a list of points bounding the single region of concern for teleseism wavefront passage.  
  
**TeleSubject** *string*    /parameters/teleseism  
The name of the CMS signal to use that alarmdist listens to.   
  
*NOTE : when pipe telesFIFO is in a dir, a grep of files which includes the FIFO pipe hangs.*  
*So locate the FIFO pipe someplace where you are not likely to be grepping files with glob '*'*  
*Do not put in /tmp, since it could be deleted while telestifle is still running when no*  
*qualfying large magnitude event occurs for weeks. But the /tmp location is ok for tests.*  

**InputFIFO** *string*      ./FIFO/telesFIFO  
The name of the FIFO from which telestifle reads event notifications. This **MUST** agree with the FIFO name used by the notification program.  

**VelocityModel** *string*  prem  
**PhaseString** *string*    P,Pdiff,PKP    
The comma-separated list of teleseismic phases to be computed by telestifle.  
  
*NOTE : for PKP traveltime is ~22 minutes?  1320 secs*  
*Phase Window end time + MaxDelay, so MaxDelay should be RT processing delay post origin like 80 secs*  
  
**MaxDelay** *integer*        80  
The maximum delay from teleseism origin time that the teleseism will be accepted by telestifle.  
  
**MinNotifyDiff** *integer*   15  
  
**PreWindow** *integer*       10  
The time before the earliest expected phase arrival that is subtracted from the start of the stifle window.  
  
**PostWindow** *integer*      10  
The time after the latest expected phase arrival that is added to the end of the stifle window.  
  
**NotifyProgram** *string*    ./msgs/telesNotify.pl  
Script, with path,  that sends both email and page messages to configured pager and email groups (dutyseis, eqalarm-local).  

**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.  
  
**DBPasswd** *string*  
The password for the database user account specified by DBUser.  
 
**Include** *string*          ../db/db_auth.d  
Include external config files. Typically, used to extract database configurations into a separate file
  

A sample *telestifle.cfg* is shown below:  
```
#
# General configuration parameters
#
CMSConfig       ../run/cfg/cms/cms.cfg
Logfile         ../run/logs/telestifle
LoggingLevel    2
ProgramName     TELE_STIFLE

#
# Application configuration parameters
#
RulesFile                       ./configs/telestifle_rules.cfg
PointsFile                      ./configs/telestifle_points.cfg
TeleSubject                     /parameters/teleseism
# NOTE when pipe telesFIFO is in a dir, a grep of files which includes the FIFO pipe hangs.
# So locate the FIFO pipe someplace where you are not likely to be grepping files with glob *
# Do not put in /tmp, since it could be deleted while telestifle is still running when no 
# qualfying large magnitude event occurs for weeks. But the /tmp location is ok for tests.
InputFIFO                       ./FIFO/telesFIFO
VelocityModel                   prem
PhaseString                     P,Pdiff,PKP
# for PKP traveltime is ~22 minutes?  1320 secs
# Phase Window end time + MaxDelay, so MaxDelay should be RT processing delay post origin like 80 secs
MaxDelay                        80
MinNotifyDiff                   15
PreWindow                       10
PostWindow                      10
NotifyProgram                   ./msgs/telesNotify.pl
Include                         ../db/db_auth.d
```  


### telestifle_rules.cfg  

This configuration file that governs the criteria to be used in making an alarm activate.  

Rules are specified by an unique rule name followed by curly brackets around any number of criteria:  
```
rule_name_1 {
    criteria1 args...
    criteria2 args...
    ...
    criterian args...
}
```  

*rule_name* is only used for logging; it is not passed to other modules.

Criteria can be one or more of :  
  
**DeltaRange** *integer* min max  
Delta (degrees) min and max, delta is measured from closest listed point in region polygon to epicenter.  
  
**DepthRange** *integer* min max  
Depth >= min and < max  
  
**MagRange** *integer* min max  
Magnitude >= min and < max  
  
If a criterion is not mentioned, any value of that quantity will qualify for that rule.  
NOTE : It takes around 5 minutes to travel 2500 km or 23 arc degs  
BC Cananda ~16 arc degs N or 1800 km  
Juneau AK is ~26 arc degs N or 2900 km  
23 deg S is between Zihuatanejo  and Acapulco MX  
25 deg S is between Acapulco and Oaxaca MX  
It takes at least 4 to 5 minutes to get first PDL origin product notification  

  
A sample *telestifle_rules.cfg* is shown below:  

```
#
# Example Teleseism Criteria file for the Teleseism Stifle process
#
# Criteria can be one or more of:
#   DeltaRange min max           - delta (degrees) min and max
#    delta is measured from closest listed point in region polygon to epicenter
#   DepthRange min max           - depth >= min and < max
#   MagRange min max             - magnitude >= min and < max
#
# If a criterion is not mentioned, any value of that quantity will qualify
# for that rule.
# NOTE is takes around 5 minutes to travel 2500 km or 23 arc degs
# BC Cananda ~16 arc degs N or 1800 km
# Juneau AK is ~26 arc degs N or 2900 km
# 23 deg S is between Zihuatanejo  and Acapulco MX
# 25 deg S is between Acapulco and Oaxaca MX
#
# It takes at least 4 to 5 minutes to get first PDL origin product notification
Direct_P1 {
        DeltaRange      2 25 
        MagRange        5.95 10.0
        DepthRange      10 1000
}

Direct_P2 {
        DeltaRange      25 104
        MagRange        6.95 10.0
        DepthRange      20 1000
}

Pdiff {
        DeltaRange      104 150
        MagRange        6.95 10.0
        DepthRange      20 1000
}

PKP {
        DeltaRange      140 180
        MagRange        6.95 10.0
        DepthRange      20 1000
}
```  
  
### telestifle_points.cfg  
  
The config lists the list of points, *not a polygon*, typically surrounding your network region. Each point is a lat and lon.  

  
**Point** *integer*  lat lon  
Single point of the region boundary  

A sample *telestifle_points.cfg* is shown below :  
  
```
# Network region boundary for Teleseism Stifle process
Point 37.43 -117.76
Point 34.50 -121.25
Point 31.50 -118.50
Point 31.50 -114.00
Point 34.50 -114.00
```
  
    
### taup-jar.env  

The **CLASSPATH** environment variable must be provided so that telestifle can find the several jar files of the TauP package.

A sample *taup-jar.env* is shown below :  

```
TAUP_JARS=/app/aqms/alarm/jars
TAUP=$TAUP_JARS/TauP-2.1.2.jar
SLF4JLOG4J12=$TAUP_JARS/slf4j-log4j12-1.7.5.jar
SEISFILE=$TAUP_JARS/seisFile-1.6.0.jar
JYTHONSTANDALONE=$TAUP_JARS/jython-standalone-2.5.2.jar
SLF4JAPI=$TAUP_JARS/slf4j-api-1.7.5.jar
LOG4J=$TAUP_JARS/log4j-1.2.17.jar
SEEDCODEC=$TAUP_JARS/seedCodec-1.0.9.jar
STAXAPI=$TAUP_JARS/stax-api-1.0-2.jar
WOODSTOXCORELGPL=$TAUP_JARS/woodstox-core-lgpl-4.1.0.jar
MYSQLCONNECTORJAVA=$TAUP_JARS/mysql-connector-java-5.1.24.jar
STAX2API=$TAUP_JARS/stax2-api-3.1.0.jar
JSAP=$TAUP_JARS/JSAP-2.1.jar
RXTX=$TAUP_JARS/rxtx-2.1.7.jar
export CLASSPATH=${TAUP}:${SLF4JLOG4J12}:${SEISFILE}:${JYTHONSTANDALONE}:${SLF4JAPI}:${LOG4J}:${SEEDCODEC}:${STAXAPI}:${WOODSTOXCORELGPL}:${MYSQLCONNECTORJAVA}:${STAX2API}:${JSAP}:${RXTX}

#You may have to edit some of the parameters to find the Java JDK on your system. 
#Look at JAVA_HOME, JNI_INCL and JNI_LIBS for possible changes. Once these are set
#properly for your system, you should be able to compile telestifle without problems.
```  

 
## DEPENDENCIES  
  
The telestifle module depends on another program to notify it of the occurrence of teleseismic earthquakes. Typically this is provided by a PDL listener program which communicates its reports through the configured FIFO.  
  
The telestifle module depends on CMS being running  

  
## MAINTENANCE  
  
No specific advice other than to check the log file.  
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-alarming/-/issues  
  
## MORE INFORMATION  
  
Authors : Pete Lombard 












