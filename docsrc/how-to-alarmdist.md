How to compile and install alarmdist {#how-to-compile-install-alarmdist}
====================================

# Pre-requisites

alarmdist depends on aqms-libs, so ensure that they are compiled and available before compiling it.

# Compile

```
make -f Makefile
```
This will create a binary named *alarmdist*.

# Install

At your preferred location, create a folder named *alarm*. Copy binary *alarmdist* under *alarm*.  
Under alarm create a folder named *configs*. This will hold all alarm configs.  Copy *alarmdist.cfg* here.



# Configure

Modify the sample *alarmdist2.cfg* as per your equirements.

Ensure that *Logfile* is available and writable to by alarmdist2.

Ensure that the database is running and reachable by alarmdist2.