How to compile and install alarmevent {#how-to-compile-install-alarmevent}
====================================

# Pre-requisites

alarmevent depends on aqms-libs, so ensure that they are compiled and available before compiling it.

# Compile

```
make -f Makefile
```
This will create a binary named alarmevent.

# Install

At your preferred location, create a folder named *alarm*. Copy binary *alarmevent* under *alarm*.  
Under alarm create a folder named *configs*. This will hold all alarm configs.  Copy *alarmevent.cfg* here.



# Configure

Modify the sample *alarmevent.cfg* as per your equirements.

Ensure that *Logfile* is available and writable to by alarmevent.

Ensure that the database is running and reachable by alarmevent.