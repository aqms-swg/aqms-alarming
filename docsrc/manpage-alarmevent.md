# alarmevent {#man-alarmevent}

alarmevent sends a CMS signal to alarmdec to re-alarm for a given event id.  

## PAGE LAST UPDATED ON

2020-06-04

## NAME

alarmevent

## VERSION and STATUS

v0.0.5 17-Jan-2013  
status: ACTIVE  

## PURPOSE

alarmevent sends a CMS signal to alarmdec to re-alarm for a given event id. The program checks that database to see that the event is present in the database before sending the signal.  

## HOW TO RUN

```
alarmevent -c alarmevent.cfg evid
```
alarmevent.cfg is described below and the evid is the integer event id of the event needing alarming.   

## CONFIGURATION FILE
Format of the config file is
```
<parameter name> <data type> <default value>
<description>
```
ALL arguments are REQUIRED.  
  
**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored.  
  
**LoggingLevel** *integer*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  
  
**LogFile** *integer*  
If 0, no logfile is generated, if 1, then a earthworm style logfile is created.  
  
**ProgramName** *string* ALARM_EVENT  
The unique name to allow this module to connect to CMS and be identifed.  
  
**CMSConfig** *string*  
Points to the cms configuration file which holds all CMS details.  
  
**UpdateSubject** *string*  
The name of the CMS signal to use for alarming.  
  
**DBService** *string*  
The name of the database being used.  
  
**DBUser** *string*  
The name of the database user account.  
  
**DBPasswd** *string*  
The password for the database user account specified by DBUser  

A sample configuration file is shown below.  
```
Logfile         ./alarmevent.log
LoggingLevel    2
ProgramName     ALARM_EVENT
CMSConfig       /app/rtem/cms/conf/cms.cfg
UpdateSubject           /signals/trimag/magnitude
DBService                       somedatabase
DBUser                          someuser
DBPasswd                        somepassword
``` 


## ENVIRONMENT

NA     

## DEPENDENCIES

The alarmevent module depends on CMS being running and the Database being available and populated with the event to be alarmed on.  

## MAINTENANCE

No specific advice other than to check the log file.  

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-alarming/-/issues

## MORE INFORMATION

[alarmdec](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdec.html), [alarmact](https://aqms-swg.gitlab.io/aqms-docs/man-alarmact.html), [alarmdist](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdist.html) are the chain of alarming programs. Alarmevent sends a signal to alarmdec. 
