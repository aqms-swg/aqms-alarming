How to compile and install sendalarm {#how-to-compile-install-sendalarm}
====================================

# Pre-requisites

sendalarm depends on aqms-libs, so ensure that they are compiled and available before compiling it.

# Compile

```
make -f Makefile
```
This will create a binary named sendalarm.

# Install

At your preferred location, create a folder named *alarm*. Copy binary *sendalarm* under *alarm*.  
Under alarm create a folder named *configs*. This will hold all alarm configs.  Copy *sendalarm.cfg* here.



# Configure

Modify the sample *sendalarm.cfg* as per your equirements.

Ensure that *Logfile* is available and writable to by sendalarm.

Ensure that the database is running and reachable by sendalarm.