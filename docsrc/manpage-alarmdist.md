# alarmdist {#man-alarmdist}

alarmdist, short for Alarm Distribution, dispatches alarm messages to users.

## PAGE LAST UPDATED ON

2020-06-03

## NAME

alarmdist

## VERSION and STATUS

v0.1.0 2018-09-19  
status: ACTIVE  

## PURPOSE


alarmdist is short for Alarm Distribution, and it is the last program in the alarming chain to run; it is also the most complicated in terms of how it runs. Alarmdist receives a CMS message from the alarmact module and the message contains the event id (evid). Upon receiving a CMS message it queries the Database for any alarm_action action entries that are PENDING or CANCELED and evaluates if it should operate on them with an action script or an undo script. The script to be run is found in the ScriptDir directory and should be a directory per action name. Inside each script directory is an action script named the same as the directory for the action (e.g. the script path would be ./alarms/Action1/Action1 if the action name is Action1 and ScriptDir is set to ./alarms) to be taken and a CANCEL_action_name script to be invoked to undo the action if the alarm was cancelled. The action_name in this case is the action written to the alarm_action table by the alarmact module.
	
The alarmdist program has 2 modes, DataCenter or RealTime. In DataCenter mode, it always runs if an alarm is invoked via CMS through alarmact. In RealTime mode alarmdist program checks the rt_role table before firing any alarm script and sees if the host on which it is running is PRIMARY and if so, immediately invokes the alarm. If the host is in SHADOW role, it then checks the peer_system_status table to see if the peer thinks its status is DOWN, and if so, then the alarmdist will act on the action and execute the appropriate script. If the alarmdist program is in RealTime mode and the peer is fine, but the host it is running on is SHADOW, then it marks the action as OVERRULED in the database and does not invoke any alarm scripts.
		
The scripts are run sequentially in alphabetical order for all of the actions necessary for a given event. The scripts also have a fixed number of seconds in which to perform their action (settable by the ScriptTimeout configuration value) or they are deemed failed.
			
When the alarmdist module starts up, it does a consistency check on its to see if there were any actions pending that need dealing with before it sits and waits for CMS messages.
				
The alarm_action database table has 5 fields, the event_id, alarm_action, action_state, modcount, and mod_time. When alarmdist updates a row in this table for each action and the action_state is set to ACTION_STATE_PENDING for a new action or ACTION_STATE_COMPLETED for being finished successfully. It sets the action_state to CANCELED if an alarm was canceled.  
					

## HOW TO RUN

```
alarmdist alarmdist.cfg
```
alarmdist.cfg is described below.  

## CONFIGURATION FILE
```
<parameter name> <data type> <default value>
<description>
```

**General configuration parameters**  

**Include** *string*  
External file to be included, with path prefix.  

**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored.  

**LoggingLevel** *number*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  

**ProgramName** *string* &nbsp;&nbsp;ALARM_DIST  
The unique name to allow this module to connect to CMS and be identifed.  

**HSInterval** *number*  
How often in seconds to send a heartbeat signal to CMS.  

**TSSOption** OR **CMSConfig** *string*  
Points to the cms configuration file which holds all CMS details. In addition, the common CMS signals should be in the config file.  

**ScriptTimeout** *number*  
The number of seconds any action script must complete within to be successful.  

**EmailGroup** *string*  
The email address to send a notification if an alarm script should fail.  


**Application configuration parameters**  

**HeartbeatSubject** *string*  
The heartbeat CMS signal that the module writes (note this is not used by any CMS receiver yet but is required).  

**ActionSubject** *string*  
The name of the CMS signal to use for springing into action (sent from alarmact).   

**CancelSubject** *string*  
The name of the CMS signal to use for springing into action for a cancellation of an alarm (sent from sendcancel).  

**Mode** *string*  
The mode of operation RT or DC (realtime or data center).  

**ScriptDir** *string*  
The directory where scripts can be found. It can be relative to where the program is executed or an absolute path.  


**Database parameters**  

**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.  

**DBPasswd** *string*  
The password for the database user account specified by DBUser.  

**DBConnectionRetryInterval** *number*  
Time to wait, in seconds, before re-attempting connection to the database.  

**DBMaxConnectionRetries** *number*  
Max number of times to attempt database connection.  

*A sample alarmdist.cfg is shown below*  

```
#
# General configuration parameters
#
CMSConfig       ../cms/conf/cms.cfg
Include         ../cms/conf/common_signals.cfg
Logfile         ../rt/logs/alarmdist
LoggingLevel    2
ProgramName     ALARM_DIST
HSInterval      30
ScriptTimeout   30

# this is for failures of the scripts that alarmdist runs, if they don't finish within ScriptTimeout secs
EmailGroup      p.friberg@isti.com


#
# Application configuration parameters
#
HeartbeatSubject                /signals/alarms/heartbeat
ActionSubject                   /signals/alarmact/action
CancelSubject                   /signals/events/cancel
Mode                            RT
ScriptDir                       ./alarms


# db stuff
Include                         ../db/db_info.d
```



## ENVIRONMENT

NA

## DEPENDENCIES

The alarmdist module depends on CMS being running and the Oracle Database being available and populated with the event to be taken action on in the alarm_action table.  

## MAINTENANCE

No specific advice other than to check the log file and the database table for actions. Here is the schema information for the alarm_action table:

```
SQL> desc alarm_action
Name                                      Null?    Type
----------------------------------------- -------- ----------------------------
EVENT_ID                                  NOT NULL NUMBER(15)
ALARM_ACTION                              NOT NULL VARCHAR2(15)
ACTION_STATE                              NOT NULL VARCHAR2(15)
MODCOUNT                                  NOT NULL NUMBER(8)
MOD_TIME                                           DATE	   
```

Alarmdist uses the peer_health tables to see if the peer is operational. It could be used by RSN's to assure that alarms go out if the primary RT system's health is determined to be poor (by either itself or by the peer server). The peer or primary would write into the shadow datbase into the peer_system_status table that the peer is 'DOWN' and not 'OPERATIONAL'. For now, we simply declare both as statically operational with the following SQL command:   

```
insert into peer_system_status values ('OPERATIONAL', sysdate);
```

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-alarming/-/issues  


## MORE INFORMATION

Authors: Patrick Small, Kalpesh Solanki, Paul Friberg, Peter Lombard  

[alarmdec](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdec.html), [alarmact](https://aqms-swg.gitlab.io/aqms-docs/man-alarmact.html), alarmdist are the chain of alarming programs. [alarmevent](https://aqms-swg.gitlab.io/aqms-docs/man-alarmevent.html) can be used to trigger an alarmdec decision process.  

