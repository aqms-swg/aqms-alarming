How to compile and install sendcancel {#how-to-compile-install-sendcancel}
====================================

# Pre-requisites

sendcancel depends on aqms-libs, so ensure that they are compiled and available before compiling it.

# Compile

```
make -f Makefile
```
This will create a binary named sendcancel.

# Install

At your preferred location, create a folder named *alarm*. Copy binary *sendcancel* under *alarm*.  
Under alarm create a folder named *configs*. This will hold all alarm configs.  Copy *sendcancel.cfg* here.



# Configure

Modify the sample *sendcancel.cfg* as per your equirements.

Ensure that *Logfile* is available and writable to by sendcancel.

Ensure that the database is running and reachable by sendcancel.