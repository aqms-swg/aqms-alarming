How to compile and install alarmdec {#how-to-compile-install-alarmdec}
====================================

# Pre-requisites

alarmdec depends on aqms-libs, so ensure that they are compiled and available before compiling it.

# Compile

```
make -f Makefile
```
This will create a binary named *alarmdec*.

# Install

At your preferred location, create a folder named *alarm*. Copy binary *alarmdec* under *alarm*.  
Under alarm create a folder named *configs*. This will hold all alarm configs.  Copy the alarmdec configs here.



# Configure

Modify the template *alarmdec.cfg*, *alarmdec_alarms.cfg* and *alarmdec_regions.cfg* as per your equirements.

Ensure that *Logfile* is available and writable to by alarmdec.

Ensure that the database is running and reachable by alarmdec.