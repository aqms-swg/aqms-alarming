
# Master Makefile for Linux 64-bit

MAKE = make
MMACROS = 

SUBDIRS = alarmact alarmdec alarmdist alarmevent sendalarm sendcancel telestifle
# Removed alarm_actions, apparently no longer used:
#    alarm_actions


bin:
	for i in ${SUBDIRS}; \
	do \
	(	echo "<<< Descending into directory: $$i >>>"; \
		cd $$i;rm -f $$i; \
		${MAKE} ${MMACROS}; \
		cd ..; ); \
	done ;\

all:
	for i in ${SUBDIRS}; \
	do \
	(	echo "<<< Descending into directory: $$i >>>"; \
		cd $$i; \
		${MAKE} ${MMACROS}; \
		cd ..; ); \
	done ;\


clean:
	for i in ${SUBDIRS}; \
	do \
	(	echo "<<< Descending into directory: $$i >>>"; \
		cd $$i; \
		${MAKE} ${MMACROS} clean; \
		cd ..; ); \
	done ;\
