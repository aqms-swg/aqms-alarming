:no_entry_sign: **CAUTION, WORK IN PROGRESS** :no_entry_sign:
# Alarming

The purpose of the alarm system is to send the proper alarms when an event is first detected or updated. Alarms actions are typically things like sending e-mails, SMS, PDL submissions, etc. However, the alarm framework could be used to initiate almost any task. The system can also cancel alarms that have been previously sent.

# Alarm modules  

The alarm system has three modules that intercommunicate via CMS messaging and the Alarm_Action table. Each module is in its own subdirectory and has its own cfg files for defining alarms and configuring CMS messaging.

**Summary of alarm modules**

* **alarmdec**   - Alarm decision, which alarms to fire when  
* **alarmact**   - Alarm action, what actions to do when an alarm fires  
* **alarmdist**  - Alarm distribution, convert an action to the firing of a script 
* **alarmdist2** - A modified alarmdist to allow parallel execution of alarm actions  
* **alarmevent** - Initiate an alarm for a given event in the db  
* **sendalarm**  - Send an alarm message of the type specified
* **sendcancel** - Initiate a cancellation of an particular alarm, or all alarms for a given event  
* **telestifle** - A program to assist in the stifling of alarms due to a teleseismic earthquake


