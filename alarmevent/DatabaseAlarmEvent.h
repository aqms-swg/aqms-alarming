/**
* @file
* @ingroup group_alarming_alarmevent
* @brief Header for DatabaseAlarmEvent.C
*/
/***********************************************************

File Name :
        DatabaseAlarmEvent.h

Original Author:
        Patrick Small

Description:


Creation Date:
        21 October 1999

Modification History:
		12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef database_alarmevent_H
#define database_alarmevent_H


// Various include files
#include <stdint.h>
#include "Database.h"


class DatabaseAlarmEvent : public Database
{
 private:

 protected:

 public:
    DatabaseAlarmEvent();
    DatabaseAlarmEvent(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseAlarmEvent();

    int EventIsValid(uint32_t evid);
};

#endif
