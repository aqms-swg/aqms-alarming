/**
* @file
* @ingroup group_alarming_alarmevent
* @brief Header for AlarmEvent.C
*/
/***********************************************************

File Name :
        AlarmEvent.h

Original Author:
        Patrick Small

Description:


Creation Date:
        21 October 1999

Modification History:
	12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef alarm_event_H
#define alarm_event_H

// Various include files
#include <stdint.h>
#include "Application.h"
#include "Connection.h"
#include "DatabaseAlarmEvent.h"


// Default configuration filename
const char DEFAULT_CONFIG[] = "./alarmevent.cfg";


class AlarmEvent : public Application
{

 private:
    char cmsfile[MAXSTR];
    uint32_t evid;
    char updatesubject[MAXSTR];
    Connection conn;
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    int checklist;
    DatabaseAlarmEvent dbase;

    int _Cleanup();
    int _SendEventSignal(uint32_t eventid);

 public:
    
    AlarmEvent();
    ~AlarmEvent();
    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
};


#endif
