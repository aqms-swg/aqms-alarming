/**
* @file
* @ingroup group_alarming_telestifle
*/
/***********************************************************

File Name :
        TeleStifle.h

Original Author:
        Pete Lombard

Description:


Creation Date:
        5 December 2012

Modification History:

	2015/12/03 v0.0.3 AWW added optional property MinNotifyDiff, a number of seconds value.
			  When seconds difference between start/end times of previous teleseism window and those of current teleseism window
			  is less than minNotifyDiff value (default=0) skip _runNotifyProgram call (prevent multiple emails for same event).
			  Changed _runNotifyProgram function argument list to include the calculated min delta for event, event src-id parsed 
			  from pdl receiver listener message, and the configured minNotifyDiff seconds value.


Usage Notes:


**********************************************************/

#ifndef teles_stifle_H
#define teles_stifle_H

// Various include files
#include <cstdio>
#include "Application.h"
#include "Connection.h"
#include "Point.h"
#include "TimeWindow.h"
#include "TSRule.h"
extern "C" {
#include "taupnative.h"
}

// Definition of a point list
typedef std::list<Point, std::allocator<Point> > PointList;

// Definition of an rule list
typedef std::list<TSRule, std::allocator<TSRule> > RuleList;

class TeleStifle : public Application
{

 private:
    char cmsfile[MAXSTR];
    char rulefile[MAXSTR];
    char pointsfile[MAXSTR];
    char telesubject[MAXSTR];
    char inputFIFO[MAXSTR];
    char velmodel[MAXSTR];
    char phaseString[MAXSTR];
    char notifyProg[MAXSTR];
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    double maxDelay;
    double minNotifyDiff;
    double preWindow;
    double postWindow;
    
    FILE *fd;
    int checklist;
    
    RuleList rules;
    PointList points;
    Connection conn;
    TauPStruct taup;
    
    int _Cleanup();
    int _SendTelesMessage(TimeWindow &win);
    int _LoadRules(const char *config);
    int _LoadPoints(const char *config);
    int _minmax(double &lat, double &lon, double &minDelta, double &maxDelta);
    int _checkRules(double &minDelta, double &depth, double &mag);
    int _getWindow(double &minDelta, double &maxDelta, double &depth,
		   TimeWindow &win);
    int _runNotifyProgram(TimeWindow &win, double datetime, double lat,
			  double lon, double depth, double mag, double delta, const char *srcid, double diff);
    
 public:

    TeleStifle();
    ~TeleStifle();
    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
};


#endif
