/**
* @file
* @ingroup group_alarming_telestifle
*/
/***********************************************************

File Name :
        TSRule.h

Original Author:
        Pete Lombard

Description:


Creation Date:
        13 November 2012

Modification History:


Usage Notes:


**********************************************************/

#ifndef tsrule_H
#define tsrule_H

// Various include files
#include "GenLimits.h"

class TSRule
{
 private:
    int valid;
    char name[MAXSTR];
    double mindepth, maxdepth;
    double minmag, maxmag;
    double mindelta, maxdelta;
    int critset;

 protected:

 public:
    TSRule();
    TSRule(const char *nm);
    TSRule(const TSRule &ts);
    ~TSRule();

    int GetName(char *nm);
    
    int SetDepthRange(double min, double max);
    int SetMagRange(double min, double max);
    int SetDeltaRange(double min, double max);
    int IsSatisfied(const double delta, const double depth, 
			const double mag, char *failreason);

    TSRule& operator=(const TSRule &ts);
    int operator!();
};

#endif
