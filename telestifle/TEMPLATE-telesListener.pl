#!/usr/bin/perl

# PDL listener exec command that feeds teleseisms to telestiffle

use strict;
use warnings;
use Qtime;

# Configurable parameters:
my $log = "/app/aqms/ProductClient/log/teles/telesListener";
my @sources = ('us','at','pt');
my $magThresh = 5.95;  # PRODUCTION VALUE
my $maxEventAgeMins = 22;  # PRODUCTION VALUE
# Make sure that $fifo points to the same file name as used by telestiffle!
my $fifo = "/app/aqms/alarm/FIFO/telesFIFO";
my $verbose = 1;
my $verbose = 1;
# End of configurable parameters

my ($sec, $min, $hr, $day, $mon, $yr) = gmtime(time);
my $logstr =  $log . '_' .  sprintf('%4d%02d%02d', $yr + 1900, $mon + 1, $day)  . '.log';
open LOG, ">> $logstr" or die "Can't open $log: $!\n";
print LOG mydate(), " new event, applying parameter filters\n";

my ($otime, $lat, $lon, $depth, $mag, $msgSource, $msgType, $msgStatus);
my $cid = '';
my $timestring;
foreach my $arg (@ARGV) {
    if ($arg =~ /--code=(\w+)/) {
	$cid = $1;
    }
    if ($arg =~ /--source=(\w\w)/) {
	$msgSource = $1;
    }
    elsif ($arg =~ /--type=(\S+)/) {
	$msgType = $1;
    }
    elsif ($arg =~ /--status=(\w+)/) {
	$msgStatus = $1;
    }
    elsif ($arg =~ /--property-latitude=(-?\d+\.?\d*)/) {
	$lat = $1;
    }
    elsif ($arg =~ /--property-longitude=(-?\d+\.?\d*)/) {
	$lon = $1;
    }
    elsif ($arg =~ /--property-depth=(-?\d+\.?\d*)/) {
	$depth = $1;
    }
    elsif ($arg =~ /--property-eventtime=(\d{4})-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d\.?\d*)/) {
	my $year = $1;
	my $month = $2;
	my $day = $3;
	my $hour = $4;
	my $minute = $5;
	my $second = $6;
	$timestring = sprintf("%4d/%02d/%02d,%02d:%02d:%06.3f",
				 $year, $month, $day, $hour, $minute, $second);
	$otime = Qtime::set_time($timestring);
    }
    elsif ($arg =~ /--property-magnitude=(\d+\.?\d*)/) {
	$mag = $1;
    }

}

my $bad = 0;
unless (defined $msgSource && defined $msgType && defined $msgStatus) {
    print LOG " msgSource undefined\n"  unless defined $msgStatus;
    print LOG " msgType undefined\n" unless defined $msgStatus;
    print LOG " msgStatus undefined\n" unless defined $msgStatus;
    print LOG "$cid incomplete message, missing one or more msg parameters\n";
    $bad = 1;
}

if ( defined $msgType && $msgType ne "origin" && $msgType ne 'internal-origin' ) {
  print LOG " $cid $msgType not [origin] or [internal-origin] , end processing\n";
  print LOG mydate(), " DONE\n\n";
  close LOG;
  exit 0;
}

if (defined $msgStatus && $msgStatus eq "DELETE") {
  if ( $verbose ) {
    print LOG " $cid status=DELETE, end processing.\n";
    print LOG mydate(), " DONE\n\n";
  }
  close LOG;
  exit 0;
}

unless (defined $otime && defined $mag && defined $lat && defined $lon
	&& defined $depth) {
    print LOG " otime undefined\n" unless defined $otime;
    print LOG " mag undefined\n" unless defined $mag;
    print LOG " lat undefined\n" unless defined $lat;
    print LOG " lon undefined\n" unless defined $lon;
    print LOG " depth undefined\n" unless defined $depth;
    print LOG "$cid incomplete message, missing one or more event parameters\n";
    $bad = 1;
}
if ($bad) {
    print LOG mydate(), " DONE\n\n";
    close LOG;
    exit 1;
}

my $msg = sprintf("%.4f %.4f %.4f %.1f %.2f", Qtime::tepoch_time($otime),
		  $lat, $lon, $depth, $mag);

if (defined $msgSource && 0 == grep { $_ eq $msgSource } @sources) {
  if ( $verbose ) {
    print LOG " $cid otime=($timestring) msg:\"$msg\",";
    print LOG " $msgSource not in ( @sources ) , end processing.\n";
    print LOG mydate(), " DONE\n\n";
  }
  close LOG;
  exit 0;
}

if (defined $mag && $mag < $magThresh) {
  if ( $verbose ) {
    print LOG " $cid otime=($timestring) msg:\"$msg\",";
    print LOG " M$mag < M$magThresh, end processing.\n";
    print LOG mydate(), " DONE\n\n";
  }
  close LOG;
  exit 0;
}

my $dsecs = time;
my $osecs = sprintf("%d", Qtime::nepoch_time($otime));
my $dmins = ($dsecs - $osecs)/60.;
if ( $dmins > $maxEventAgeMins ) {
  if ( $verbose ) {
    print LOG " $cid otime=($timestring) msg:\"$msg\",";
    print LOG " event too old " . sprintf("%.1f",$dmins) . " min., end processing.\n";
    print LOG mydate(), " DONE\n\n";
  }
  close LOG;
  exit 0;
}

if (-p $fifo) {
    unless (open FH, "> $fifo") {
	print LOG "Can't open $fifo: $!\n";
        print LOG " $cid otime=($timestring) msg:\"$msg\",  NOT SENT\n";
        print LOG mydate(), " DONE\n\n";
	close LOG;
	exit 1;
    }
}
else {
    print LOG "$fifo does not exist or is not a FIFO\n";
    print LOG " $cid otime=($timestring) msg:\"$msg\", NOT SENT\n";
    print LOG mydate(), " DONE\n\n";
  
    close LOG;
    exit 1;
}

print LOG " $cid otime=($timestring) [$msgType] msg:\"$msg\", SENT TO FIFO\n";
print FH $msg." $cid\n";
close FH;
print LOG mydate(), " DONE\n\n";
close LOG;
exit 0;

sub mydate {
    my ($sec, $min, $hr, $day, $mon, $yr) = gmtime(time);
    return sprintf('%02d/%02d/%4d %02d:%02d:%02d',
		   $mon + 1, $day, $yr + 1900, $hr, $min, $sec);
}
