/**
* @file
* @ingroup group_alarming_telestifle
* @brief Header for DatabaseTelestifle.C
*/
/***********************************************************

File Name :
        DatabaseTelestifle.h

Original Author:
        Pete Lombard

Description:


Creation Date:
        13 November 2012

Modification History:


Usage Notes:


**********************************************************/

#ifndef database_telestifle_H
#define database_telestifle_H


// Various include files
#include "Database.h"

class DatabaseTelestifle : public Database
{
 private:

 protected:

 public:
    DatabaseTelestifle();
    DatabaseTelestifle(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseTelestifle();

    int DoSendAlarms();
};

#endif
