#! /bin/bash

if [ -z "$RT_HOME" -o -z "$LD_LIBRARY_PATH" ]; then source /usr/local/etc/aqms.env; fi

if [ -n "$AQMS_HOME" ]; then
  home="$AQMS_HOME"
else
  home="$RT_HOME"
fi

logdir="$RT_LOGS"
if [ -z "$logdir" ]; then
  logdir="$home/run/logs"
fi

cd $home/alarm

cfgdir="configs"
bindir='.'

if [ -z "$CLASSPATH" ]; then
  taupenv="$cfgdir/taup-jar.env"
  echo "Checking for taup env file: $taupenv"
  if [ -f "$taupenv" ]; then
    source $taupenv
    #echo CLASSPATH:"$CLASSPATH"
    if [ -z "$CLASSPATH" ]; then
      echo "Error CLASSPATH for taup jars undefined exiting..."
      exit 1
    fi
  fi
fi

ctlcfgfile="$cfgdir/$(basename $0 .sh).cfg"

timestamp=`date -u +'%F %T %Z'`

# declare functions
start() {
    pid=`pgrep -x 'telestifle'`
    if [ -n "$pid" ]; then
      echo "telestifle process already running @ $timestamp"
      echo "`pgrep -l -f 'telestifle' | sort -k 3`"
      exit
    fi

    # start the telestifle processes
    $bindir/telestifle $cfgdir/telestifle.cfg 2>&1 | conlog $logdir/telestifle_console &

    sleep 2

    status
}

stop() {
    for pname in telestifle
    do
      for i in `pgrep -x $pname`
      do
        echo "killing $pname pid $i @ $timestamp"
        kill $i
        sleep 2
      done
      # in case SIGTERM did not work 
      for i in `pgrep -x $pname`
      do
        kill -9 $i
      done
    done
    return 0
}

restart() {
    stop
    sleep 3
    start
}

status() {
    pids=`ps -ef | egrep -e 'telestifle' | grep -v 'egrep -e' | sort -k 9`
    timestamp=`date -u +'%F %T %Z'`
    if [ -n "$pids" ]; then
       echo "telestifle process running @ $timestamp"
       echo "$pids"
       exit 0
    else
       echo "No telestifle process found @ $timestamp"
       exit 1
    fi
}

usage() {
    echo "Telestifle init script"
    echo "Usage: $(basename $0) [start|stop|restart|status|help]"
    echo "  telestifle forwards teleseismic start end time window message to alarmdec."

    exit $1
}

check_status() {
  local pids=''
  for x in telestifle
  do
      #echo "pids=ps -ef | egrep -e \"$x.*${x}.cfg\" | grep -v 'egrep -e' | sort -k 9"
      pids=`ps -ef | egrep -e "$x.*${x}.cfg|${x}_console" | grep -v 'egrep -e' | sort -k 9`
      print_status $x '' "$pids"
  done
}

print_status() {
  timestamp=`date -u +'%F %T %Z'`
  local prog=$1
  local ctype="$2"
  local pids="$3"
  if [ -n "$pids"  ]; then
   echo "$prog $ctype processes running @ $timestamp"
   echo "$pids"
  else
    echo "No $prog $ctype processes found @ $timestamp"
  fi
}
###############################################################################################
# End of function declarations
if [ $1 == '-h' -o $1 == '--help' ]; then
  usage 0;
fi
# Execute input action via function call
case $1 in
  start)
    start ;;
  stop)
    stop ;;
  restart)
    restart ;;
  status)
    #status ;;
    check_status ;;
  help)
    usage 0 ;;
  *)
    echo "Unknown or missing input argument: $1"
    usage 2 ;;
esac

