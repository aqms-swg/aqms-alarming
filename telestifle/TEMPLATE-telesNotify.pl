#!/usr/local/bin/perl

# Sample perl script for teleseism stiffle notification.
# The script will be run by telestiffle with the following syntax:
# (path...)/telesNotify win_start win_end
# where win_start is the teleseism window start time,
# win_end is the window end time. 
# Both times are in the format is YYYY/MM/DD,HH:MM:SS
#

use strict;
use Qtime;

exit main();

sub main {
    # Configurable parameters:
    my @pagerProg = ("/path/to/pager", #--FIXME--
		     "/path/to/qpage");        #--FIXME--
    my $mailProg = "/usr/bin/mailx";   #--FIXME--
# for testing:
    my @pagerRecipients = ("some_recipient", #--FIXME--
			  );
    my @mailRecipients = ("some_user\@blahmail.com", #--FIXME--
			 );
# for ucbrt:
#::    my @pagerRecipients = ("some-pager-alert",
#::			  );
#::    my @mailRecipients = ("user1\@blahmail.com",
#::			  "user2\@blahmail.gov",
#::			  "user3\@blahmail.com",
#::			  "mailing-list-1\@blahmail.com",
#::			 );


    # End of configurable parameters

    die "Usage: $0 win_start(YYYY/MM/DD,HH:MM:SS) win_end [lat lon depth mag]\n"
      unless (2 <= scalar @ARGV);

    # Snip off the YMD for brevity
    my $winStart = substr $ARGV[0], 11;
    my $winEnd = substr $ARGV[1], 11;
    my ($datetime, $lat, $lon, $depth, $mag);
    if (7 == scalar @ARGV) {
	$datetime = $ARGV[2];
	$lat = $ARGV[3];
	$lon = $ARGV[4];
	$depth = $ARGV[5];
	$mag = $ARGV[6];
    }
    my @message = ("Tele-stifle window $winStart - $winEnd.",
		   "Duty Seismologist should review all events in window on DRP.");
    my $subject = "Tele-Stifle Window";

    my $status = 0;

    # email message; separate lines
    my $recipList = join ",", @mailRecipients;
    my $cmd = "$mailProg -s \"$subject\" $recipList";
    open FH, "| $cmd" or die "Error starting $cmd: $!\n";
    print FH join("\n", @message), "\n";
    if (defined $mag) {
	print FH "\nOrigin time: ", timestr($datetime);
	print FH "\nLatitude:    $lat\n";
	print FH "Longitude:   $lon\n";
	print FH "Depth:       $depth\n";
	print FH "Magnitude:   $mag\n";
    }
    unless (close FH) {
	$status = 1;
	print STDERR "mail command failed; $!\n";
    }

    # pager message, all on one line
    if (0 == scalar @pagerRecipients) {
	return $status;
    }
    my $pagerProg;
    if (-x $pagerProg[0]) {
	$pagerProg = $pagerProg[0];
    } elsif (-x $pagerProg[1]) {
	$pagerProg = $pagerProg[1];
    } else {
	die "Can't find a usable pager program\n";
    }
    $recipList = join ",", @pagerRecipients;
    $cmd = "$pagerProg $recipList " . join " ", @message;
    `$cmd`;
    if ($?) {
	$status = 1;
	print STDERR "pager command failed: $!\n";
    }

    return $status;
}

sub timestr {
    my $time = shift;

    my $qt = Qtime::set_time("${time}T");
    my @ary = Qtime::mtime($qt);
    return sprintf("%4d/%02d/%02d,%02d:%02d:%02d",
                   $ary[0], $ary[1], $ary[2],
                   $ary[3], $ary[4], $ary[5]);
}
