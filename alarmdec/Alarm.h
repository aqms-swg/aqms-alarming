/**
* @file
* @ingroup group_alarming_alarmdec
* @brief Header for Alarm.C
*/
/***********************************************************

File Name :
        Alarm.h

Original Author:
        Patrick Small

Description:


Creation Date:
        21 September 1999

Modification History:

01/07/2008 Leapsecond changes by Kalpesh Solanki


Usage Notes:


**********************************************************/

#ifndef alarm_H
#define alarm_H

// Various include files
#include <vector>
#include <string>
#include "GenLimits.h"
#include "TimeStamp.h"
#include "Duration.h"
#include "Region.h"
#include "TimeWindow.h"
#include "Event.h"
#include "Origin.h"

#define MAG_MASK_UNKNOWN           0x000001
#define MAG_MASK_NONE              0x000002
#define MAG_MASK_PRIM_AMP          0x000004
#define MAG_MASK_BODYWAVE          0x000008
#define MAG_MASK_ENERGY            0x000010
#define MAG_MASK_LOCAL             0x000020
#define MAG_MASK_TRAD_UCB_LOCAL    0x000040
#define MAG_MASK_NET_UCB_LOCAL     0x000080
#define MAG_MASK_LG                0x000100
#define MAG_MASK_PRIM_CODA         0x000200
#define MAG_MASK_SURFWAVE          0x000400
#define MAG_MASK_MOMENT            0x000800
#define MAG_MASK_LOW_GAIN          0x001000
#define MAG_MASK_BENIOFF           0x002000
#define MAG_MASK_DUR               0x004000
#define MAG_MASK_HELIO             0x008000
#define MAG_MASK_LOCAL_REDUCED     0x010000


class Alarm
{
 private:
    int valid;
    char name[MAXSTR];
    Region region;
    double mindepth, maxdepth;
    double minmag, maxmag;
    double minmaxgap, maxmaxgap;
    double minlocrms, maxlocrms;
    double minmagrms, maxmagrms;
    double minmagquality, maxmagquality;
    int minnumlocphases, maxnumlocphases;
    int minnumrecphases, maxnumrecphases;
    int minnummagsta, maxnummagsta;
    int magtypemask;
    Duration eventage;
    TimeWindow active;
    int reviewmask;
    int critset;
    std::vector<string> etypes;
    std::vector<string> gtypes;
    std::vector<string> oauths;
    std::vector<string> mauths;

 protected:

 public:
    Alarm();
    Alarm(const char *nm);
    Alarm(const Alarm &a);
    ~Alarm();

    int GetName(char *nm);

    int SetRegion(Region &r);
    int SetEventAge(const Duration &dur);
    int SetReviewMask(char *mask_string);
    int SetEtype(char *etype_string);
    int SetGtype(char *gtype_string);
    int SetOauth(char *auth_string);
    int SetMauth(char *auth_string);
    int SetDepthRange(double min, double max);
    int SetNumLocPhasesRange(int min, int max);
    int SetNumRecPhasesRange(int min, int max);
    int SetLocRMSRange(double min, double max);
    int SetMaxGapRange(double min, double max);
    int SetMagRange(double min, double max);
    int SetMagRMSRange(double min, double max);
    int SetMagQualityRange(double min, double max);
    int SetNumMagStationsRange(int min, int max);
    int SetMagTypeMask(char *mask_string);
    int SetActive(const TimeWindow &win);

    int IsSatisfied(const Event &e, char *failreason);

    Alarm& operator=(const Alarm &a);
    int operator!();
};

#endif
