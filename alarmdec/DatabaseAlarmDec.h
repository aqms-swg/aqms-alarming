/**
* @file
* @ingroup group_alarming_alarmdec
* @brief Header for DatabaseAlarmDec.C
*/
/***********************************************************

File Name :
        DatabaseAlarmDec.h

Original Author:
        Patrick Small

Description:


Creation Date:
        24 September 1999

Modification History:
		12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef database_alarmdec_H
#define database_alarmdec_H

// Various include files
#include <stdint.h>
#include "Event.h"
#include "Database.h"


class DatabaseAlarmDec : public Database
{
 private:
    int _GetMagnitude(uint32_t magid, Magnitude &mag);
    int _GetOrigin(uint32_t orid, Origin &org);

 protected:

 public:
    DatabaseAlarmDec();
    DatabaseAlarmDec(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseAlarmDec();

    int GetEvent(uint32_t evid, Event &e);
};

#endif
