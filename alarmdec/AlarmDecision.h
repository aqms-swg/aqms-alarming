/**
* @file
* @ingroup group_alarming_alarmdec
* @brief Header for AlarmDecision.C
*/
/***********************************************************

File Name :
        AlarmDecision.h

Original Author:
        Patrick Small

Description:


Creation Date:
        20 September 1999

Modification History:
		12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef alarm_decision_H
#define alarm_decision_H

// Various include files
#include <list>
#include <stdint.h>
#include "RTApplication.h"
#include "DatabaseAlarmDec.h"
#include "Alarm.h"
#include "Region.h"

// Definition of an alarm list
typedef std::list<Alarm, std::allocator<Alarm> > AlarmList;


// Definition of an alarm list
typedef std::list<Region, std::allocator<Region> > RegionList;



class AlarmDecision : public RTApplication 
{

 private:
    char heartbeatsubject[MAXSTR];
    char eventsubject[MAXSTR];
    char updatesubject[MAXSTR];
    char telessubject[MAXSTR];
    char alarmsubject[MAXSTR];
    char stdAlarmfile[MAXSTR];
    char altAlarmfile[MAXSTR];
    char regionfile[MAXSTR];
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;
    int telesInEffect;
    int haveAlternateRules;
    
    AlarmList stdAlarms;
    AlarmList altAlarms;
    TimeWindow telesWindow;
    
    int _Cleanup();
    int _LoadAlarms(const char *config, RegionList &rl, AlarmList &al);
    int _LoadRegions(const char *config, RegionList &rl);
    int _SendAlarmMessage(uint32_t evid, char *altype);

 public:
    
    AlarmDecision();
    ~AlarmDecision();
    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
    int DoEvent(uint32_t evid);
    int DoWindow(double start, double end);
};


#endif
