/**
* @file
* @ingroup group_alarming_alarmdec
* @brief Header for HeartbeatMessageHandler.C
*/
/***********************************************************

File Name :
        HeartbeatMessageHandler.h

Original Author:
        Kalpesh Solanki

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_HEARTBEAT messages.


Creation Date:
        21 July 2003

Modification History:


Usage Notes:


**********************************************************/

#ifndef heartbeat_msg_handler_H
#define heartbeat_msg_handler_H


// Various include files
#include "Connection.h"


// Function prototype for the alarm message handler
void handleHeartbeatMsg(Message &m, void *arg);


#endif
