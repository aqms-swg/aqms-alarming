/**
* @file
* @ingroup group_alarming_alarmdec
* @brief Header for TelesWindowHandler.C
*/
/***********************************************************

File Name :
        TelesWindowHandler.h

Original Author:
        Pete Lombard

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_TELES_WINDOW messages.


Creation Date:
 	9 November 2011

Modification History:


Usage Notes:


**********************************************************/

#ifndef teles_window_handler_H
#define teles_window_handler_H


// Various include files
#include "Connection.h"


// Function prototype for teleseism window message handler
void handleTelesMsg(Message &m, void *arg);


#endif
