/**
* @file
* @ingroup group_alarming_alarmact
* @brief Header for DatabaseAlarmAct.C
*/
/***********************************************************

File Name :
        DatabaseAlarmAct.h

Original Author:
        Patrick Small

Description:


Creation Date:
        24 September 1999

Modification History:
		12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef database_alarmact_H
#define database_alarmact_H


// Various include files
#include <list>
#include <stdint.h>
#include "Action.h"
#include "Database.h"


// Definition of an alarm list
typedef std::list<Action, std::allocator<Action> > ActionList;


class DatabaseAlarmAct : public Database
{
 private:

    int _GetModCount(uint32_t evid, Action &action);

 protected:

 public:
    DatabaseAlarmAct();
    DatabaseAlarmAct(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseAlarmAct();

    int WriteActions(uint32_t evid, ActionList &al);
};

#endif
