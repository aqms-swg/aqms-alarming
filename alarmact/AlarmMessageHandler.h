/**
* @file
* @ingroup group_alarming_alarmact
* @brief Header file for AlarmMessageHandler.C
*/
/***********************************************************

File Name :
        AlarmMessageHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_ALARM messages.


Creation Date:
        24 September 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef alarm_msg_handler_H
#define alarm_msg_handler_H


// Various include files
#include "Connection.h"


// Function prototype for the alarm message handler
void handleAlarmMsg(Message &m, void *arg);


#endif
