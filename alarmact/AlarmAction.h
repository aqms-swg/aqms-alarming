/**
* @file
* @ingroup group_alarming_alarmact
* @brief Header for AlarmAction.C
*/
/***********************************************************

File Name :
        AlarmDecision.h

Original Author:
        Patrick Small

Description:


Creation Date:
		20 September 1999

Modification History:
		12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef alarm_action_H
#define alarm_action_H

// Various include files
#include <map>
#include <stdint.h>
#include <string>
#include "RTApplication.h"
#include "DatabaseAlarmAct.h"


// Definition of an alarm list
typedef std::map<std::string, ActionList, std::less<std::string>, std::allocator<std::pair< const std::string, ActionList> > > AlarmMap;



class AlarmAction : public RTApplication 
{
 private:
    char heartbeatsubject[MAXSTR];
    char alarmsubject[MAXSTR];
    char actionsubject[MAXSTR];
    char actionfile[MAXSTR];
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;

    AlarmMap alarms;

    int _Cleanup();
    int _LoadActions(const char *config);
    int _SendActionSignal(uint32_t evid, ActionList::iterator alp);

 public:

    AlarmAction();
    ~AlarmAction();
    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
    int DoAlarm(uint32_t evid, char *altype);

};


#endif
