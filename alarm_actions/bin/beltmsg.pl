#!/usr/bin/perl
#
# Write a belt page summary of event 
#
# DEBUG option   -x : prints primary query text and exits
#
# STYLE 
#------------------------------------------------------------------
# 1.2 13:40 09/04 34 15 118 24 08 CI b 3 mi. SE  of Pacoima, CA 15401369 N=11 RMS=0.24 ERH=1.06 Ml N=006 RMS=0.35
#------------------------------------------------------------------
use strict;
use warnings;

use DBI;            # Load the DBI module

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;

# parse optional switches (N.B. this 'shift's the @ARGV array) 
use Getopt::Std;
our ( $opt_2, $opt_d, $opt_h, $opt_k, $opt_x);
getopts('2d:hkx') or usage();

if ( $opt_h ) { usage(); }

# database alias to use for connection
my $db = $masterdb;
if ($opt_d) { $db = $opt_d; }

my $id = $ARGV[0];

# Build query string
my $sql = "Select e.evid,o.orid,n.magid,n.magnitude,n.magtype,n.nsta,n.uncertainty,o.auth,
    TrueTime.getString(o.datetime),o.lat,o.lon,o.depth,o.erhor,o.ndef,o.wrms,o.gap,o.nbfm,
    o.quality,e.etype,o.rflag,o.subsource,e.selectflag,o.bogusflag,e.version,
    WHERES.LOCALE_BY_TYPE(o.lat,o.lon,o.depth,1,'town')
    from Event e,Origin o,NetMag n WHERE (e.prefor=o.orid(+)) and (e.prefmag=n.magid(+))
    and (e.evid=$id)\n";

if ( $opt_x ) {
  print "$sql\n";
  exit 0;
}

# Create db connection 
my $dbconn = DbConn->new($db)->getConn();
my $sth = $dbconn->prepare( $sql ) or die "Error preparing sql\n:$sql\n";

$sth->execute() or die "Query failed\n:$sql" ; 

# Read the column results into separate variables          
while (my @data = $sth->fetchrow_array()) {
    my $ii = 0;

    my $evid = $data[$ii++] || 0;
    my $orid = $data[$ii++] || 0;
    my $magid = $data[$ii++] || 0;
    my $mag = $data[$ii++] || 0;
    my $magtype = $data[$ii++] || ' ';
    my $msta =  $data[$ii++] || 0;
    my $mrms =  $data[$ii++] || 0;
    my $oauth = $data[$ii++] || ' ';
    my $timstr = $data[$ii++] || 0;
    my $lat = $data[$ii++] || 0;
    my $lon = $data[$ii++] || 0;
    my $z = $data[$ii++] || 0;
    my $errh = $data[$ii++] || 0;
    my $nph = $data[$ii++] || 0;
    my $rms = $data[$ii++] || 0;
    my $gap = $data[$ii++] || 0;
    my $nfm = $data[$ii++] || 0;
    my $oqual = $data[$ii++] || 0;

    my $etype = $data[$ii++] || ' ';
    my $rflag = $data[$ii++] || ' ';
    my $src = $data[$ii++] || ' ';
    my $selectflag = $data[$ii++] || 0;
    my $bogusflag = $data[$ii++] || 0;
    my $vers = $data[$ii++] || 0;
    my $town = $data[$ii++] || 'null';
    #print "$town\n";
    # parse/compose town string
    #1234567890123456789012345678901234567890123456789012345678901234567890
    #   5.8 km (   3.6 mi) SE  ( 129. azimuth) from Diamond Bar, CA
    my $km = substr($town, 0,6);
    $km *= .621371;
    $errh *= .621371;
    my $az = substr($town, 22, 3);
    $az =~ s/ //g;
    my $ref= substr($town, 47);  
    $town = sprintf ("%.0f mi %s of %s", $km, $az, $ref);
    
    $magtype = 'x' if $magtype eq 'un';

    # handle precision formatting
    # NOTE: sprintf BUG: f1.1 of 3.05 comes out at "3.0" not "3.1" unless I do this! DDG 1/15/03
    $mag += .00001; 
    my $smag = ( $opt_2 ) ? sprintf("%03.2f", $mag) : sprintf("%01.1f", $mag);
    my $latd = int ($lat);
    my $latm = sprintf("%0.2d",($lat - $latd)*60.);
    my $lond = int ($lon);
    my $lonm = sprintf("%0.2d",($lon - $lond)*60.);
    $lond = -$lond if $lond < 0;
    $lonm = -$lonm if $lonm < 0;

    my $srms = sprintf "%01.2f", $rms;
    my $smad = sprintf "%01.2f", $mrms;
    my $ssrc = sprintf "%3.3s", $src;
    my $sgap = sprintf("%0.3d", $gap);
    my $sz = sprintf("%02.0f", $z);

    my $squal = 'a';
    if ( $oqual > .75 ) {
       $squal = 'a';
    }
    elsif ( $oqual > .5 ) {
       $squal = 'b';
    }
    elsif ( $oqual > .25 ) {
       $squal = 'c';
    }
    else {
       $squal = 'd';
    }
     
    my $time = substr($timstr,11,5);
    my $date = substr($timstr,5,5);
    my $prefix = ( $opt_k ) ? "CANCELLED:" : "";
    printf "%s%4.4s %4s %4s %02d %02d %02d %02d %02d %2s %1s %s %d N=%d RMS=%s ERH=%.2f M%s N=%03d RMS=%s\n",
            $prefix,$smag,$time,$date,$latd,$latm,$lond,$lonm,$sz,$oauth,$squal,$town,$id,$nph,$srms,$errh,$magtype,$msta,$smad;
#From QPager.C:
# 1.2 13:40 09/04 34 15 118 24 08 CI b 3 mi. SE  of Pacoima, CA 15401369 N=11 RMS=0.24 ERH=1.06 Ml N=006 RMS=0.35
#%01s%3.1lf %02.2d:%02.2d %02.2d/%02.2d %2.2d %02d %3.3d %2.2d %02.2d CI b %s %07d N=%02d RMS=%04.2f ERH=%04.2f %s N=%03d RMS=%1.2f"
    
} # end of "while" loop
if ($sth->rows == 0) {
    print "No event matched for input evid $id\n\n";
}

$sth->finish if $sth;

### Now, disconnect from the database
$dbconn->disconnect
  or warn "Disconnection failed: $DBI::errstr\n";

exit 0;

#
# Help message about this program and how to use it
#
sub usage {
    print <<"EOF";

    Usage: $0 [-h] [-d dbase] [-2] [-x] <evid>

    Prints an event summary line that is used for text of page (SMS) type alarms.
     
     -h        : print this usage info

     -d dbase  : connect to named database (defaults to "$masterdb")

     -k        : adds a "CANCELLED:" prefix to output string, for use by CANCEL_ alarm scripts.

     -2        : print 2-decimal digit magnitude value

     -x        : for debugging SQL only, prints database sql query string then exits

    example: $0 -d mydb 123
EOF
   exit;
}

