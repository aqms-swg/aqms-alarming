#!/usr/bin/perl

#
# Print magtype, magnitude, distance to closest town, time of event, origin subsource, and event version 
#
use strict;
use warnings;

use DBI; # Load the DBI module

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;
use Event;

# parse optional switches
use Getopt::Std;
our ( $opt_a, $opt_D, $opt_d, $opt_f, $opt_h, $opt_i, $opt_I, $opt_l, $opt_m, $opt_o, $opt_T, $opt_v, $opt_z );
getopts('aDd:f:hiIlmoTvz');

# info only, a no-op
if ($opt_h or @ARGV == 0) { usage(); }

# default to masterdb, else use option
my $db = $masterdb;
if ($opt_d) { $db = $opt_d };

# Test connection
my $dbh = DbConn->new($db)->getConn() or die "Unable to create connection to $db\n";

# required event evid input argumment
my $evid = $ARGV[0];

# retrieve event data from db
my $event = Event->new($evid, $db) or die "Failed for $evid $db\n";
if ($event->{_isEmpty} ) {
  print "Event $evid not found in $db\n";
  exit;
}

my ($sql, $sth); # re-usable variables

# stored package distance units defaults to km
my $dunits = 'km';
if ( $opt_m ) { # configure package for distance miles units
  $sql = "BEGIN WHERES.dist_units_miles; END;";
  $sth = $dbh->prepare("$sql");
  $sth->execute;
  $sth->finish;
  $dunits = 'mi'
}

# query for the dist, az, place data for closest town to event
# WHERES.town( p_lat, p_lon, p_z, p_dist, p_az, p_elev, p_place)
$sql = "BEGIN WHERES.town( ?, ?, ?, ?, ?, ?, ?); END;";
$sth = $dbh->prepare("$sql");
$sth->bind_param(1, $event->{_lat});
$sth->bind_param(2, $event->{_lon});
$sth->bind_param(3, 0);
my ($dist, $az, $elev, $place);
$sth->bind_param_inout(4, \$dist, 100);
$sth->bind_param_inout(5, \$az, 100);
$sth->bind_param_inout(6, \$elev, 100);
$sth->bind_param_inout(7, \$place, 100);
$sth->execute;
$sth->finish;
#printf("%5.1f %5.1f %5.0f %s",$dist,$az,$elev,$place); 

# compose compass cardinal point azimuth direction string
my $azs = '?';
$sql = "BEGIN ? := WHERES.compass_pt(?); END;";
$sth = $dbh->prepare("$sql");
$sth->bind_param_inout(1, \$azs, 3);
$sth->bind_param(2, $az);
$sth->execute();
$sth->finish;

# local or UTC time query
my $dtFormat = 'HH:mm:ss zzz yyyy-MM-dd'; 
if ( $opt_f ) {
  $dtFormat = $opt_f
}
$dtFormat = $dbh->quote($dtFormat);
if ( $opt_l ) { # local
  $sql = "select UTIL.epochtostring( ?, $dtFormat, m.text) from messagetext m
          where m.texttype='LocalTimeZone' and m.srcnet=?";
  $sth = $dbh->prepare("$sql");
  $sth->execute( ($event->{_ot} + 0.5), $event->{_eauth} );
}
else { # UTC
  $sql = "select UTIL.epochtostring( ?, $dtFormat, 'UTC') from dual";
  $sth = $dbh->prepare("$sql");
  $sth->execute( $event->{_ot} + 0.5 );
}

# get datetime and timezone strings returned by query
my $time = '?';
while (my @data = $sth->fetchrow_array()) {
  $time = $data[0] || '?';
}
$sth->finish;

# disconnect from the database
$dbh->disconnect or warn "Disconnection failed: $DBI::errstr\n";

# compose mag string value
my $smag =  '?';
if ($opt_T) {
  $smag = sprintf("%.1f", $event->{_magObject}->{_magnitude}) . ' ' . 'M' . $event->{_magObject}->{_magtype};
}
else {
  if ( defined $event->{_magObject}->{_magtype}  && defined $event->{_magObject}->{_magnitude} ) {
    $smag = 'M' . $event->{_magObject}->{_magtype} . ' ' . sprintf("%.1f", $event->{_magObject}->{_magnitude}); 
  }
}

# format distance value
my $sdist = sprintf('%.1f',$dist);

# check to add optional degress azimuth 
$azs =~ s/\s+$//; # trim trailing blanks
if ( $opt_D ) {
  my $deg = `echo '\xb0'`;
  chomp $deg;
  $azs .= "(" . sprintf("%.0f%s",$az,$deg) . ")";
}

my $auth = '';
if ( $opt_a ) { $auth = $event->{_eauth}; }

my $version = '';
if ( $opt_v ) { $version  = "-v" . $event->{_version}; }

my $_id = '';
my $id_ = '';
if ( $opt_i ) {
  $id_ = "$evid" . "$auth ";
}
elsif ( $opt_I ) {
  $_id = " $evid" . "$auth";
}

my $src = ( $event->{_src} =~ 'RT' ) ? 'RT' : 'PP'; 

# print event summary subject line (without linefeed)
$place = '' if ! defined $place;
$place =~ s/, /,/;
if ( $opt_o ) { # time at start
  if ( $opt_z ) {
    my $depth = ( $opt_m ) ? sprintf( "z=%.1f mi", $event->{_z}*.6214 ) : sprintf( "z=%.1f km", $event->{_z} ); 
    print "$time $id_($smag) $sdist $dunits $azs of $place $depth $src$version$_id"; 
  }
  else {
    print "$time $id_($smag) $sdist $dunits $azs of $place $src$version$_id"; 
  }
}
else { # time/id at end
  if ( $opt_z ) {
    my $depth = ( $opt_m ) ? sprintf( "z=%.1f mi", $event->{_z}*.6214 ) : sprintf( "z=%.1f km", $event->{_z} ); 
    print "$id_($smag) $sdist $dunits $azs of $place $depth $src$version $time$_id"; 
  }
  else {
    print "$id_($smag) $sdist $dunits $azs of $place $src$version $time$_id"; 
  }
}

exit;

# -----------------------------------------------------------
# Help message about this program and how to use it
#
sub usage {
    print <<"EOF";

    $0 [-d database]  <evid> 
    Print magtype, magnitude, distance to closest town, time of event, origin subsource, and event version 
    -h        : print usage info
    -a        : append the event auth to evid (used with -i or -I option)
    -d dbase  : use this particular db (defaults to "$masterdb")
    -D        : append azimuthal degrees to the compass cardinal direction, e.g. ENE(77)
    -f format : java datetime format, default='HH:mm:ss zzz yyyy-MM-dd' (quote strings with spaces)
    -i        : prepend event evid at beginning
    -I        : append event evid at end 
    -l        : report origin in local time, default is UTC time
    -o        : put origin time before magnitude, default is magnitude first
    -m        : distance units is miles, default is km
    -T        : put magtype tag after magnitude value, default is before
    -v        : report version number after origin subsource
    -z        : report depth of hypocenter after place and time 

EOF
exit;
}
