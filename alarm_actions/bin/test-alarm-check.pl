#!/usr/bin/perl
#use lib "/opt/local/lib/perl5/site_perl";
use warnings;
use strict;
use DBI;
use Time::Local;

#Configuration Parameters
#=======================================================
#Database Configuration File (containing DBService/DBUser/DBPasswd)
my $dbconfig = "$ENV{RT_HOME}/db/db_auth.d";

#Full path to mailx bin
my $mailxbin = "mailx";

#RT Log directory
my $logdir = "$ENV{RT_LOGS}";

#Email recipient address
my $emailaddr = "eqalarm_local";

#Full path to tnpage bin
#my $pageBin = "$ENV{RT_HOME}/utils/bin/tnpage";
my $pageBin = "$ENV{RT_HOME}/utils/paging/page.py";

#Full path to tnpage config
my $pageConfig = "";

#Page destinations (comma separated list)
my $pageDest = "-g dutyseis -m";

#Alarm to check for
my $alarmName = "EVTPRM2PDL";
#========================================================

my $evid = $ARGV[0];
if($evid eq ""){
  print "Usage: \$failmsg.pl <evid>\n";
  exit(-1);
}

#Read config file
open (CONFIGFILE,$dbconfig) || die "Can't open $dbconfig";
my $db = "";
my $username = "";
my $password = "";

while(my $line = <CONFIGFILE>){
 # print "$line\n";
  if($line =~ /^\s*DBService\s+(.+)\s*$/){
    $db = trim($1);
  }
  elsif($line =~ /^\s*DBUser\s+(.+)\s*$/){
    $username = trim($1);
  }
  elsif($line =~ /^\s*DBPasswd\s+(.+)\s*$/){
    $password = trim($1);
  }
}

if($db eq "" or $username eq "" or $password eq "") {
  print "Unable to read the database connection parameters from $dbconfig\n";
  exit(-1);
}

my $driver = "Oracle";
my $command = "dbi:$driver:$db";

# Connect to the database
my $dbh = DBI->connect($command, $username, $password);
if ($dbh eq "undef") {
  die "Error: " . $DBI::errstr;
}

my $sth = $dbh->prepare(
                      qq(
                         select count(*) from alarm_action a where a.event_id = ?
                         and a.alarm_action = ? and action_state != 'ERROR'
                      )
                     );
#$sth->bind_param( 1, $evid);
#$sth->bind_param( 2, $alarmName);
$sth->execute($evid, $alarmName);
my $action_count = 0;

while(my @info = $sth->fetchrow_array()){
  $action_count = $info[0];
}

my $sth2 = $dbh->prepare(qq(select TrueTime.getEpoch(o.datetime,'NOMINAL'),TrueTime.getString(o.datetime),
                            n.magtype,n.magnitude
                            from origin o, event e left outer join netmag n
                            on n.magid=e.prefmag where e.evid = o.evid and e.prefor = o.orid and e.evid = ?));

$sth2->execute($evid);

my $orgtime = 0;
my $orgstr = '';
my $mag = '';
my $mtype = '';
while(my @info = $sth2->fetchrow_array()){
  $orgtime = $info[0];
  $orgstr = $info[1];
  $mtype = $info[2];
  $mag = $info[3];
}

$dbh->disconnect();

print "Alarm Actions: $action_count\n";

if($action_count < 1){
 #grep logs for the event
  my $filename = getLogFileName();
  my $begin_regex = "Received signal for event $evid";
  my $end_regex = "EVENTEND $evid";
  my $msg_regex = "Checking alarm";
  my $grep_flag = 0;
  my $log_content = "THIS IS A TEST, IGNORE -AWW 2015/02/17";

  print "log :$filename\n";
  if (open (LOG,$filename) ) {
    while(my $line = <LOG>){
        if($line =~ /^(\d\d:\d\d:\d\d:\d\d\d)\|\s*(.*)\s*$/){
           my $logline = $2;
           #print "$logline\n";
           if($grep_flag == 1){
             if($line =~ /^.*$msg_regex.*$/){
               $log_content .= $line;
             }
             elsif($line =~ /^.*$end_regex.*$/){
               last;
             }
           }
           if($line =~ /^.*$begin_regex\s*$/){
             $grep_flag = 1;
           }
        }
    }
  }
  else {
    #die "Can't open log $filename for $ARGV[0]";
    print "Can't open log $filename for $ARGV[0]\n";
  }

  print "$log_content\n";
  my $subject = "TEST-PAGE-MAIL 4 MISSING/ERROR $alarmName alarm ($evid: M$mtype $mag $orgstr)";
  sendMail($log_content, $subject);
  sendPage($subject);
}
exit(0);

sub getLogFileName {
  #Check if its time for a new file
  my $filename = "";
  my ($sec,$min,$hour,$mday,$mon,$year,$a,$b,$c) = gmtime($orgtime);
  $mon++;
  $year += 1900;
  my $val = $mday;
  if($val < 10){
    $mday = "0$mday";
  }
  $val = $mon;
  if($val < 10){
    $mon = "0$mon";
  }

  $filename = "$logdir/alarmdec\_$year$mon$mday.log";
  return $filename;
}

sub sendMail {
    my ($msg,$subject) = @_;
    my $mailtxt = "Event $evid $alarmName alarm failed. \n$msg\n";
    my $date = `date -u`;
    my $tmp = "/tmp/alarm_fail_report.txt";

    open(TMPFILE,"> $tmp") or die "Error opening :" . $tmp;
    printf(TMPFILE "$mailtxt\n$date");
    close(TMPFILE);

    system("$mailxbin -s \"$subject\" $emailaddr  < $tmp");
    print "Mail report sent to $emailaddr \@" . gmtime() . "\n";
}

sub sendPage {
  my $msg = $_[0];
  my $cmd = "$pageBin $pageDest \"$msg\"";
  #print "$0: Executing page:$cmd\n";
  system($cmd);
  print "$0: Error executing page:$cmd\n" if ($? != 0);
}

sub trim {
    my @out = @_;
    for (@out) {
        s/^\s+//;
        s/\s+$//;
    }
    return wantarray ? @out : $out[0];
}
