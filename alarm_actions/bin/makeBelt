#!/usr/bin/perl

use strict;
use DBI;
use File::Basename;
use Getopt::Std;

# Global variables
our($dbName, $dbUser, $dbPass, $dbh, $VERSION, $NetID, $Agency_Abbrev );
our($opt_c, $opt_d, $opt_h );

$VERSION = "0.0.1";

##############################################################################
#	print_syntax	- print syntax and exit.
##############################################################################
sub print_syntax {
    my ($cmdname) = @_;
    print << "EOF";	
    $cmdname version $VERSION
	$cmdname - create belt event or cancel format
Syntax:
	$cmdname -c config [-d] eventID [version]
	$cmdname -h
where:
	-c config   - specify config file; required
	-h	    - prints this help message.
        -d          - create the delete format instead of event format

EOF
   exit(0);
}


exit main();

# The main function
sub main {

    my ($me) = basename($0);

# Process command-line arguments
    my $status = 0;

    getopts ('c:dh');
    
    print_syntax($me) if ($opt_h);
    print_syntax($me) unless ($opt_c);

    print_syntax($me) unless (scalar @ARGV > 0);
    my ($evid, $alarmVersion, $belt);
    $evid = $ARGV[0];
    $alarmVersion = $ARGV[1] if (scalar @ARGV > 1);
    
    parse_config($opt_c);
    
    if ($opt_d) {
	$belt = sprintf("Event %04d is not an earthquake\n", $evid);
    }
    else {
	$dbh = DBI->connect("dbi:Oracle:" . $dbName, $dbUser, $dbPass, 
			    {RaiseError => 1, PrintError => 0, AutoCommit => 0});
	my $sth = $dbh->prepare(q{
	    BEGIN
		SELECT truetime.getString(o.datetime) datetime, o.lat, o.lon, 
	        o.depth, e.version, o.ndef, o.wrms, o.gap, o.distance,
	        o.erhor, o.sdep, o.rflag, n.magnitude, n.magtype
		INTO :datetime, :lat, :lon, :depth, :version, :nphase, :rms,
		 :gap, :nearsta, :erhor, :sdep, :rflag, :mag, :magtype
		FROM event e, origin o, netmag n
		WHERE e.prefor = o.orid AND e.prefmag = n.magid
		AND e.evid = :evid;
	    wheres.town(:lat, :lon, 0.0, :dist_t, :az_t, :elev, :place_t);
	    :dir_t := wheres.compass_pt(:az_t);
	    wheres.bigtown(:lat, :lon, 0.0, :dist_b, :az_b, :elev, :place_b);
	    :dir_b := wheres.compass_pt(:az_b);
	    END;
	});
	
	my ($mag, $magtype, $lat, $lon, $depth, $datetime);
	my ($distkm, $distmi, $dir, $place, $dist_t, $az_t, $place_t, $dir_t);
	my ($dist_b, $az_b, $place_b, $dir_b, $elev);
	my ($version, $nphase, $rms, $gap, $nearsta, $erhor, $sdep, $rflag);
	$sth->bind_param(":evid", $evid);
	$sth->bind_param_inout(":lat", \$lat, 100);
	$sth->bind_param_inout(":lon", \$lon, 100);
	$sth->bind_param_inout(":mag", \$mag, 100);
	$sth->bind_param_inout(":magtype", \$magtype, 100);
	$sth->bind_param_inout(":depth", \$depth, 100);
	$sth->bind_param_inout(":datetime", \$datetime, 100);
	$sth->bind_param_inout(":version", \$version, 100);
	$sth->bind_param_inout(":nphase", \$nphase, 100);
	$sth->bind_param_inout(":gap", \$gap, 100);
	$sth->bind_param_inout(":rms", \$rms, 100);
	$sth->bind_param_inout(":rflag", \$rflag, 100);
	$sth->bind_param_inout(":nearsta", \$nearsta, 100);
	$sth->bind_param_inout(":erhor", \$erhor, 100);
	$sth->bind_param_inout(":sdep", \$sdep, 100);
	$sth->bind_param_inout(":elev", \$elev, 100);
	$sth->bind_param_inout(":dist_t", \$dist_t, 100);
	$sth->bind_param_inout(":az_t", \$az_t, 100);
	$sth->bind_param_inout(":place_t", \$place_t, 100);
	$sth->bind_param_inout(":dir_t", \$dir_t, 100);
	$sth->bind_param_inout(":dist_b", \$dist_b, 100);
	$sth->bind_param_inout(":az_b", \$az_b, 100);
	$sth->bind_param_inout(":place_b", \$place_b, 100);
	$sth->bind_param_inout(":dir_b", \$dir_b, 100);
	$sth->execute();
	$dbh->disconnect();
	
	if (!defined $lat or !defined $lat or !defined $mag
	    or !defined $depth or !defined $datetime) {
	    print STDERR "No data found for event $evid\n";
	}
	
# The town table may not include the bigtown entries, so we check both
# and pick the closer one.
	if ($dist_t < $dist_b) {
	    $distkm = $dist_t;
	    $dir = $dir_t;
	    $place = $place_t;
	}
	else {
	    $distkm = $dist_b;
	    $dir = $dir_b;
	    $place = $place_b;
	}

	$dir =~ s/\s*(\w+)\s*/$1/;
	$distmi = $distkm * 0.62137;
	my $loc = sprintf "%.1f km (%.1f mi) $dir of $place", $distkm, $distmi;
	my $latd = int(abs($lat));
	my $latm = 60.0 * (abs($lat) - $latd);
	my $lond = int(abs($lon));
	my $lonm = 60.0 * (abs($lon) - $lond);
	$depth = sprintf("%02d", ($depth < 100) ? $depth : 99);
	
	$belt = sprintf ("M%s%3.1f %s %02d:%02d %03d:%02d %02d %s %s %s " .
			"V=%d N=%d RMS=%0.2f AZ=%d DM=%d EH=%0.1f EZ=%0.1f R=%s %s\n",
			 $magtype, $mag,
			 $datetime, $latd, $latm, $lond, $lonm, $depth,
			 $loc, uc($NetID), $evid,
			 (defined $version) ? $version : $alarmVersion, $nphase,
			 $rms, $gap, $nearsta, $erhor, $sdep, $rflag,
			 $Agency_Abbrev);
    }

    print $belt;

    return $status;
}

sub parse_config {
    my $config = shift;

    open CF, "< $config" or die "Error opening $config: $!\n";
    while (<CF>) {
        if (/^\s*DBUser\s+(\S+)/) {
            $dbUser = $1;
        }
        elsif (/^\s*DBPasswd\s+(\S*)/) {
            $dbPass = $1;
        }
        elsif (/^\s*DBService\s+(\S*)/) {
            $dbName = $1;
        }
        elsif (/^\s*NetID\s+(\S*)/) {
            $NetID = $1;
        }
        elsif (/^\s*Agency_Abbrev\s+(\S*)/) {
            $Agency_Abbrev = $1;
        }
    }
    die "DBUser missing\n" unless (defined $dbUser);
    die "DBPasswd missing\n" unless (defined $dbPass);
    die "DBService missing\n" unless (defined $dbName);
    die "NetID missing\n" unless (defined $NetID);
    die "Agency_Abbrev missing\n" unless (defined $Agency_Abbrev);
    return;
}
