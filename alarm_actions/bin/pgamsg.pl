#!/usr/bin/perl

# Print strong motion (sm)  ampset amp data associated with evid

use strict;
use warnings;

use DBI;

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;

# parse optional switches (N.B. this 'shift's the @ARGV array) 
use Getopt::Std;
our ( $opt_d, $opt_h, $opt_t, $opt_x );
getopts('d:htx');

if ($opt_h) { usage(); }

if ( @ARGV == 0 ) { usage(); }

# Default to masterdb cfg
my $dbase = $masterdb;
if ($opt_d) { $dbase = $opt_d; }  

my $evid = $ARGV[0];
if (!($evid =~ /^\d+$/) ) {
  print "$0 Error: <id> must be a number: $evid\n";
  exit 2;
 } 

# Create db connection handle
my $dbh = DbConn->new($dbase)->getConn();

# Default to wildcard all amptypes
my $amptype = 'PGA';


# Create SQL prepared statement to retrieve amp data
my $sql =
   "select 
    WHERES.separation_km(c.lat, c.lon, o.lat, o.lon) dist, truetime.getString(o.datetime) ot,
    a.net, a.sta, a.seedchan, a.location, a.amplitude, a.amptype, a.units, a.subsource,
    TrueTime.getString(a.datetime)
    from Amp a, Ampset s, AssocEvAmpset e, Channel_Data c, Origin o, Event t where
    e.evid = ? and
    e.isvalid = 1 and
    e.subsource = 'ampgen' and
    s.ampsetid = e.ampsetid and
    a.ampid = s.ampid and
    a.amptype = ? and
    t.evid = e.evid and
    o.orid = t.prefor and
    a.net = c.net and a.sta = c.sta and a.seedchan = c.seedchan and a.location=c.location and
    c.offdate > SYS_EXTRACT_UTC(SYSTIMESTAMP)
    order by a.amplitude desc, 1 asc, a.net,a.sta,a.seedchan,a.location
   "; 

my $sth = $dbh->prepare( $sql );
print "$sql\n" if $opt_x; 

# execute the prepared statment. 
$sth->execute($evid, $amptype); 

my $header = "nt sta   chn lc   amp     typ    unit  src       dist    time (utc)";
if ($opt_t) {print "$header\n";}

# create an array to hold query data
my @data;
my $ii = 0;
my %stas = ();
my $str = '';
my $ot = '';
#18:50 08/25

my $icnt = 0;
while (@data = $sth->fetchrow_array()) {
    # put array results into separate variables          
    $ii = 0;
    my $dist  = $data[$ii++];
    $ot = $data[$ii++] || '?';
    my $net = $data[$ii++] || '?';
    my $sta = $data[$ii++] || '?';
    my $comp = $data[$ii++] || '?';
    my $loc   = $data[$ii++] || '?';
    $loc =~ s/ /-/g;
    my $amp = $data[$ii++];
    my $type = $data[$ii++] || '?';
    my $units = $data[$ii++] || '?';
    if ( $units eq "cmss" ) {
      $units = '%g';
      $amp = $amp/9.81;
    }
    my $src = $data[$ii++] || '?';
    my $dtStr = $data[$ii++] || '?';
    
        
    $loc =~ tr/[\ ]/[\-]/;        # change loc spaces to dashes
          
    # handle potential null numbers. The || construct doesn't work for these since
    # a variable will take on the alternate value if the returned value = 0.0
    if (not defined $amp)  { $amp  = -999; }
    if (not defined $dist) { $dist = -999; }

    my $key ="$net". "." . "$sta"; 
    if ( not exists $stas{$key} )  {
      #$stas{"$key"} = $str;
      $stas{"$key"} = "1";
      $str .= sprintf "%-2s %s %-3s %-2s %06.2f %%g ", $net, $sta, $comp, $loc, $amp;
      $icnt++;
    }
    last if $icnt >= 8;
}
  
if ($sth->rows == 0) {
    $str = "PGA no amps found $evid\n";
}
else {
    $str = 'PGA ' . substr($ot, 11, 5) . ' ' . substr( $ot, 5, 5) . ' ' .  $str;
}
$sth->finish;

print "$str\n";

# Now, disconnect from the database
$dbh->disconnect
  or warn "Disconnection failed: $DBI::errstr\n";

exit;

# -----------------------------------------------------------------
sub usage {
    print <<"EOF";

    Print strong motion PGA amp data associated with one event in AssocEvAmpset.

    Amps are ordered by PGA amplitude and distance,net,sta,location,seedchan,amptype.

    Syntax: $0 [-h] [-d dbase] [-t] <evid>
     
     -h         : print this usage info
     -d dbase   : db alias to use (defaults to "$masterdb")
     -t         : print column header title
    
    example: $0 -d k2db 9277430
EOF
    exit;
}

