#!/usr/bin/env python3
import sys
import os
import socket
import argparse
import array
import re
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

USERS_FILE='/app/aqms/utils/paging/db/users.cfg'
SERVICES_FILE='/app/aqms/utils/paging/db/services.cfg'
GROUPS_FILE='/app/aqms/utils/paging/db/groups.cfg'

class PageUsers:

    def __init__(self):
        pass

    def ReadAndParseUsersFile (self, name):
        self.name = name
        with open (USERS_FILE, 'rt') as inFile:
            text = inFile.readlines()

        pattern = self.name + ' {\n'
        count = 0
        for line in text:
            count = count + 1
            if line == pattern:
                pin     = (re.sub ("PrimaryPIN ", "", text[count])).strip()   #.split()
                service = (re.sub ("Service ", "", text[count + 1])).strip()  #.split()


        ''' split will remove all spaces from pin and service '''
        ''' ex:                                               '''
        #x = ''.join (pin.split())
        #y = ''.join (service.split())
        return (pin, service)

    def ReadAndParseServicesFile (self, serviceAddr):
        self.serviceAddr = serviceAddr
        with open (SERVICES_FILE, 'rt') as inFile:
            text = inFile.readlines()

        pattern = self.serviceAddr + ' {\n'
        count = 0
        for line in text:
            count = count + 1
            if line == pattern:
                proto = text[count]
                maxConnRetries = text[count + 1]

        return (proto.split(), maxConnRetries.split())

    def ReadAndParseGroupsFile (self, groupName):
        self.groupName = groupName
        temp  = []
        users = []
        with open (GROUPS_FILE, 'rt') as inFile:
            text = inFile.readlines()
  
        pattern = self.groupName + ' {\n'
        count = 0
        for line in text:
            count = count + 1
            if line == pattern:
                while text[count] != '}\n':
                    temp.append (text[count])
                    count = count + 1

        for i in temp:
            if i[0] != '#':
                users.append (i.split()[1])

        return users
        
    def SendPage (self, pin, message, address):
        msg = MIMEMultipart('alternative')
        self.pin     = ''.join (pin.split())
        text         = message
        self.message = message
        self.address = address
        self.hname   = socket.gethostname()
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(self.message, 'html')
        #if ((len (self.message)) > 120):    # twitter length :)
        #    msgtxt = MIMEText (self.message[:120], 'html')
        #    msg.attach (text)
        #    msg.attach (msgtxt)
        #    #msg = email.mime.text.MIMEText (self.message[:120])
        #else:
        #    msgtxt = MIMEText (self.message, 'html')
        #    msg.attach (text)
        #    msg.attach (msgtxt)
        #    #msg = email.mime.text.MIMEText (self.message)
        toStr = "%s@%s" %(self.pin, self.address) 
        #msg['Subject'] = "RT page from %s" % (self.hname)
        msg['From'] = "aqms@%s" % (self.hname)
        me = "aqms@%s" % (self.hname)
        you = toStr
        msg['To'] = toStr
        msg.attach(part1)
        msg.attach(part2)
        s = smtplib.SMTP ('localhost')
        #s.send_message (msg)
        s.sendmail (me, you, msg.as_string())
        s.quit()

class Usage():
    ''' Print out the usage of pdaemon.py '''
    pass

class Main ():
    parser = argparse.ArgumentParser (description='Program for sending pages to users')
    parser.add_argument ('-s',  '--server',        help = 'Set host',                  required = False)
    parser.add_argument ('-p',  '--port',          help = 'Set port',                  required = False)
    parser.add_argument ('-u',  '--user',          help = 'Set user',                  required = False)
    parser.add_argument ('-g',  '--group',         help = 'Set group',                 required = False)
    parser.add_argument ('-m',  '--message',       help = 'Message to send',           required = True)
    parser.add_argument ('-sf', '--services_file', help = 'directory + services file', required = False)
    parser.add_argument ('-uf', '--users_file',    help = 'directory + services file', required = False)
    parser.add_argument ('-gf', '--groups_file',   help = 'directory + services file', required = False)
    args = parser.parse_args()

    if (args.services_file and os.path.isfile (args.services_file)):
        global SERVICES_FILE 
        SERVICES_FILE = args.services_file

    if (args.users_file and os.path.isfile (args.users_file)):
        global USERS_FILE 
        USERS_FILE = args.users_file

    if (args.groups_file and os.path.isfile (args.groups_file)):
        global GROUPS_FILE
        GROUPS_FILE = args.groups_file

    # Instantiate class
    page = PageUsers()

    # Either a group or a user needs to be specified as an argument
    # If user was specified as an argument
    if (args.user):
        users = args.user.split(',')
        for user in users:
          (number, service) = page.ReadAndParseUsersFile (user.strip())
          (proto, maxConnRetries) = page.ReadAndParseServicesFile (service)
          #print(number)
          #print(proto)
          #print(maxConnRetries)
          #print ('Send page to {0} {1}'.format( repr(user), repr(args.message) ))
          page.SendPage (number, args.message, proto[2])
    # If group was specified as an argument
    if (args.group):
        groups = args.group.split(',')
        for group in groups:
            users = page.ReadAndParseGroupsFile (group.strip())
            for user in users:
               (number, service) = page.ReadAndParseUsersFile (user)
               (proto, maxConnRetries) = page.ReadAndParseServicesFile (service)
               #print(number)
               #print (user)
               #print (proto)
               #print ('Send page to {0} {1}'.format( repr(user), repr(args.message) ))
               page.SendPage (number, args.message, proto[2])

if __name__ == "__main__":
    Main()

