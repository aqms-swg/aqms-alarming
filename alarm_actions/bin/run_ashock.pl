#!/usr/bin/perl
#
# this script sends the Aftershock email as an addon to the Simpson page
#
# Mandy Johnson 030611
#
# Add the explicit pathnames for scp and ssh 
#

# fixed for galena 2005-06-23 paulf


$eventid = @ARGV[0];
$eventid =~ s/\.forecast$//;
$eventsrc = @ARGV[1];

system ("/usr/bin/scp /app/aqms/alarm/forecasts/tmp/$eventid.forecast galena:/home/web/forecast/$eventid.forecast");
 
system ("/usr/bin/ssh galena /home/web/forecast/wrapper.pl /home/web/forecast/$eventid.forecast");

 open(NEW, ">/app/aqms/alarm/forecasts/$eventid.html");
 print NEW "LI";

# Print the event id with leading zero 
#
# print NEW "$1";
 printf NEW ("%08d", $eventid);
 print NEW "CI01 afterwarn http://www.scsn.org/forecast/$eventid.html CALTECH/USGS AFTERSHOCK PROBABILITY REPORT"
;
 close(NEW);

system ("cp /app/aqms/alarm/forecasts/$eventid.html /app/aqms/EIDS/polldir/$eventid.forecast");
# Commented out for EIDS switch -sms 4-aug-2010
###system ("cp /app/aqms/alarm/forecasts/$eventid.html /app/aqms/QDDS/polldir/$eventid.forecast");

end
