# AA_Simpson_Map  

Prepare Simpson Map products (CUBE message and a waveform LI file), push them to a EIDS server over the network or satellite link; for cancellation, submit delete message.   
  
**Dependencies**  
  
*  cubeMessage.pl  
*  QDDSwavelink.pl



  
