# EVTPRM2PDL, CANCEL_EVTPRM2PDL  

Prepare the event parametric products (quakeml and nearby-cities), push them to a PDL server; for cancellation, submit delete message to PDL.  
  
**Dependencies**  
  
*  [qml](https://gitlab.com/aqms.swg/aqms-utilities/-/tree/master/qml)  
*  nearby-cities.pl ???
*  PDL