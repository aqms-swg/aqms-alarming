# A_Shakemap_Vn  

Send a "shake_alarm" message a shakemap server (usually on local network); for cancellation, submit "shake_cancel" message to the shakemap server.  

If you have multiple shakemap servers, create a corresponding file to connecting to each such server. For example, A_Shakemap_V1 connects to host name shakemap-server-1:port1, A_Shakemap_V2 connects to host name shakemap-server-2:port1 etc.  
