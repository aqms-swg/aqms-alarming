# Alarm Actions   
  
Alarm actions in this directory follow a convention:  
  
1.  The Action name is both the directory name and the script/program name within that directory.

2. The Cancel script/program for a given action is named the same as the action script, but with CANCEL_ prepended to the script name.

3. All the action and cancel scripts/programs take two arguments: the event ID and the action "mod" count (number of times the action has been called out to be executed or cancelled.  
  
Most actions are written in Perl, though they can be written in any programming language of choice.  


The various AQMS alarm actions are described below. Any dependencies that the alarm actions may have are present under *bin* and the corresponding configs are present under *config*.   
  

## AA_Simpson_Map, CANCEL_AA_Simpson_Map  

Prepare Simpson Map products (CUBE message and a waveform LI file), push them to a EIDS server over the network or satellite link; for cancellation, submit delete message.   
  
Scripts used :  
*  cubeMessage.pl  
*  QDDSwavelink.pl  


## A_Shakemap_V1, CANCEL_AA_Simpson_Map    

Send a "shake_alarm" message a shakemap server (usually on local network); for cancellation, submit "shake_cancel" message to the shakemap server.  

If you have multiple shakemap servers, create a corresponding file to connecting to each such server. For example, A_Shakemap_V1 connects to host name shakemap-server-1:port1, A_Shakemap_V2 connects to host name shakemap-server-2:port1 etc.  
  
  
## AmpExportPDL  

Alarm script for the unassociated amps Distribution process into PDL

Scripts used :    
*  [ampExport](../alarm_actions/ampExport)  


## BeltPager, CANCEL_BeltPager, Group_Belt_Page, CANCEL_Group_Belt_Page, GroupBeltPageTS, CANCEL_GroupBeltPageTS      

NCSS Belt page script for the Alarm Distribution process. BeltPager sends belt page message to a user or group; Group_Belt_Page sends belt page sends page message as SMS to a user or group. GroupBeltPageTS does corresponding actions for stifling telesiesms.  
  
Scripts used :  
*  **makeBelt** : Create belt event or cancel message.  
*  **run_beltmsg** : Wrapper shell script for makeBelt.  
*  **paging/page.py** : Send SMS to user and/or groups of users.    

  

## CUBE2EIDS, CANCEL_CUBE2EIDS  

CUBE to EIDS alarm actions  
  
Scripts used :  
*  **makeCUBE** : create CUBE E or DE format using stored procedures.  
  

## CUBE2QDDS, CANCEL_CUBE2QDDS  

CUBE to QDDS alarm actions  
  
Scripts used :  
*  **makeCUBE** : See above.  
  

## Duty_Amp_Page, CANCEL_Duty_Amp_Page  

Send a strong motion ampset amp data message; for cancellation, send cancel message.  

Scripts used :    
*  **pgamsg.pl** : Print strong motion (sm)  ampset amp data associated with evid.  
*  **run_pgamsg** : Wrapper shell script which calls pgamsg.pl.  
*  **paging/page.py** : Send SMS to user and/or groups of users.  
  


## DutySeisPageTS, CANCEL_DutySeisPageTS  

Send belt page summary message to group dutyseis for teleseisms; for cancellation, submit cancel message.  
  
Scripts used :    
*  **beltmsg.pl** : Write a belt page summary of event 
*  **run_beltmsg** : Wrapper shell script which calls beltmsg.pl.  
*  **paging/page.py** : Send SMS to user and/or groups of users.



## EVTPRM2PDL, CANCEL_EVTPRM2PDL  

Prepare the event parametric products (quakeml and nearby-cities), push them to a PDL server; for cancellation, submit delete message to PDL.  
  
Scripts used :    
*  [qml](https://gitlab.com/aqms-swg/aqms-utilities/-/tree/master/qml)  
*  nearby-cities.pl ???
*  PDL  
  
  
## Group_Cell_Page, CANCEL_Group_Cell_Page, GroupCellPageTS, CANCEL_GroupCellPageTS, Special_Pages, CANCEL_Special_Pages      
  
Send belt page message as email to a group.  For cancellation, submit cancel message. GroupCellPageTS is for stifling teleseisms. Group_Cell_Page and GroupCellPageTS send email to eqpager. Special_Pages sends email to special.
  
Scripts used :  
*  **beltmsg.pl** : Write a belt page summary of event.
*  **run_beltmsg** : Wrapper shell script which calls beltmsg.pl.  
*  **emailLocalSubject.pl** : Send belt page message as email.
  

## NCSSMail, CANCEL_NCSSMail  
  
Send Internal (not for public) email using stored procedures.  For cancellation, submit cancel message.
  
Scripts used :  
*  **makeIntMail** : create and send Internal (not for public) email using stored procedures.  

 

## NoOP  
  
No operation alarm  
  

## STEP_LINK, CANCEL_STEP_LINK  

Compose and send EIDS link "LI" message.  
  
Scripts used :  
*  **run_EIDS_LImsg** : Compose EIDS link "LI" message.  
  

## StiflePostproc, CANCEL_StiflePostproc  
  
Send an ALARMSTIFLE message as email to stifle alarms in AQMS post processing systems. For cancellation, send cancel message.  
  
Scripts used :  
*  **post.pl** : ???
*  **beltmsg.pl** : Write a belt page summary of event.
*  **run_beltmsg** : Wrapper shell script which calls beltmsg.pl.  
*  **emailLocalSubject.pl** : Send belt page message as email.  


## Trig_Email, CANCEL_Trig_Email  
  
Send email when a new trigger is inserted into the databaes. For cancellation, send cancel message.  
  
Scripts used :  
*  **getEtype.pl** : ???
*  **catone.pl** :  ???


## TweetQuake  

Send belt page message as a tweet.  

Scripts used :  
*  **beltmsg.pl** : Write a belt page summary of event.
*  **run_beltmsg** : Wrapper shell script which calls beltmsg.pl.  
*  **TweetQuake** : Binary used to send a tweet. Based on [twty](https://github.com/mattn/twty)
  
  
## Z_Ashock_Email, CANCEL_Z_Ashock_Email  

Send aftershock email as an addon to the Simpson page.  For cancellation, send cancel message.

Scripts used :  
*  **run_ashock.pl** :  Script sends the aftershock email as an addon to the Simpson page.   
  

## ZZ_Alarm_Check, CANCEL_ZZ_Alarm_Check   
  
Check the status of an alarm in the database and send message via email or SMS.  
   
Scripts used :  
*  **failmsg.pl** : Check status of an alarm.  
*  **paging/page.py** : Send SMS to user and/or groups of users.  
*  **test-alarm-check.pl** : Check status of an alarm and send test message.  
  

## Alarms for TMTS2  

The next four alarms are for TMTS2.  

TMTS publishes CMS messages to indicate that a moment magnitude or moment tensor mechanism has been inserted into the data
base. It is up to the alarming system to handle these CMS messages and to take the appropriate actions. To assist with tha
t process, several alarm action scripts and support scripts are included in the *aqms-utilities/alarm_actions/TMTS2/action
s*, *aqms-utilities/alarm_actions/TMTS2/bin* directories.  
  
All of these script need to be configured for your environment.  

### MTlocalmail, CANCEL_MTlocalmail  

Send email to local recipients about a new or cancelled moment tensor solution. Uses *mtmail* to do the real work.  
  

### MTmail, CANCEL_MTmail  

Send email to recipients about a new or cancelled moment tensor solution. Uses mtmail to do the real work. Only the configuration file names make these different from *MTlocalmail* and *CANCEL_MTlocalmail*. You may choose to use different alarming criteria to activate these two sets of actions if you want.  

Scripts used :   
*  **mtmail** : Extracts the *email.txt* file from the MecObject database table and sends it to a configured list of recipients. Requires a config file  (see the sample aqms-utilities/alarm_actions/conf/mtmail.cfg) as well as a file listing recipient addresses (one per line).  
*  **extractMTfile** : Extract any of the files stored in the MecObject table for an event.   


### MTweb, CANCEL_MTweb  

Prepare the moment tensor web products (html and image files), push them a web server and submit CUBE LI (link) messages to QDDS; for cancellation, submits CUBE link delete message to QDDS. Uses *mtweb* script for the actual work.  

Scripts used :     
*  **mtweb** : Prepares and distributes the web products and QDDS messages for a moment tensor. Besides its config file, there are several parameters near the top of this script need to be configured for your environment. See the sample ???TMTS/alarming/conf/mtweb.cfg???. The html pages from this script depend on two CSS style sheets; these can be generated at installation time by running the script with the "-s" option. Copy the style style sheets "screen.css" and "print.css" to the directory on your web server from with the MT products will be served.   


### MTweb4PDL, CANCEL_MTweb4PDL  

Prepare the moment tensor web products (html and image files), push them to a PDL server; for cancellation, submits CUBE link delete message to QDDS. Uses *mtweb* script with *-P* options for the actual work.  
  
Scripts used :    
* **mtweb** : See above
  



  


  
  



  

  


