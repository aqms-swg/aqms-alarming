/**
* @file
* @ingroup group_alarming_sendcancel
* @brief Header for DatabaseSendCancel.C
*/
/***********************************************************

File Name :
        DatabaseSendCancel.h

Original Author:
        Patrick Small

Description:


Creation Date:
        22 October 1999

Modification History:
		12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef database_sendcancel_H
#define database_sendcancel_H


// Various include files
#include <stdint.h>
#include "Database.h"


class DatabaseSendCancel : public Database
{
 private:

 protected:

 public:
    DatabaseSendCancel();
    DatabaseSendCancel(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseSendCancel();

    int ActionIsValid(uint32_t evid, const char *action);
};

#endif
