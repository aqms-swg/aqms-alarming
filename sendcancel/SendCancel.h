/**
* @file
* @ingroup group_alarming_sendcancel
* @brief Header for SendCancel.C
*/
/***********************************************************

File Name :
        SendCancel.h

Original Author:
        Patrick Small

Description:


Creation Date:
        22 October 1999

Modification History:
		12 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef send_cancel_H
#define send_cancel_H

// Various include files
#include <stdint.h>
#include "RetCodes.h"
#include "Application.h"
#include "Connection.h"
#include "DatabaseSendCancel.h"
#include "RTStatusManager.h"


// Default configuration filename
const char DEFAULT_CONFIG[] = "./sendcancel.cfg";


class SendCancel : public Application
{
 private:
    char tssfile[MAXSTR];
    uint32_t evid;
    char aname[MAXSTR];
    char cancelsubject[MAXSTR];
    Connection conn;
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    char responsesubject[MAXSTR];
    bool tssretry;

    int dbinterval;
    int dbretries;
    int force;
    int checklist;
    DatabaseSendCancel dbase;

    int _Cleanup();
    int _SendCancelMessage();
    int _SendReliableCancelMessage();

 public:
    
    SendCancel();
    ~SendCancel();
    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
};

#endif
